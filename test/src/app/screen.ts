export class Screen {
  constructor(
    public title: string,
    public description: string,
    public slug: string,
    public cast_id: number
  ) {}
}
