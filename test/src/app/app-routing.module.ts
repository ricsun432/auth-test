import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  {
    path: 'login',
    loadChildren: () =>
      import('./profile/profile.module').then((m) => m.ProfileModule),
  },
  {
    path: 'home',
    loadChildren: () =>
      import('./resource/resource.module').then((m) => m.ResourceModule),
  },
  {
    path: 'cast-editor',
    loadChildren: () => import('./cast/cast.module').then((m) => m.CastModule),
  },
  {
    path: 'create-exam',
    loadChildren: () =>
      import('./exams/exams.module').then((m) => m.ExamsModule),
  },
  {
    path: 'create-course',
    loadChildren: () =>
      import('./course/course.module').then((m) => m.CourseModule),
  },
  { path: '**', component: PageNotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
