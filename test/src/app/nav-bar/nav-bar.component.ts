import { Component, OnInit } from '@angular/core';
import { NavbarService } from 'src/app/navbar.service';
import { HttpService } from '../http.service';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css'],
})
export class NavBarComponent implements OnInit {
  constructor(private httpService: HttpService, public nav: NavbarService) {}

  ngOnInit(): void {}

  logout() {
    this.httpService.postLogOut().subscribe();
  }
}
