export class Choice {
  constructor(
    public audio_url?: string,
    public image_url?: string,
    public is_correct?: boolean,
    public long_input?: string,
    public order_id?: number,
    public short_input?: string
  ) {}
}
