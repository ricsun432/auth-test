export class Block {
  constructor(
    public order: number,
    public type: string,
    public properties: object
  ) {}
}
