export class SecBlock {
  constructor(
    public title: string,
    public cover_uri: string,
    public content?: any
  ) {}
}
