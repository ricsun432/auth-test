import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Exam } from 'src/exam';
import { Resource } from 'src/resource';
import { Block } from './block';
import { Choice } from './choice';
import { Collection } from './collection';
import { Course } from './course';
import { Item } from './item';
import { Screen } from './screen';
import { Section } from './section';
import { SecBlock } from './section-block';

@Injectable({
  providedIn: 'root',
})
export class HttpService {
  constructor(private http: HttpClient) {}

  getCourse(course_uuid: string): Observable<any> {
    return this.http
      .get(
        `https://staging.gateway.buri.dev/services/buricourse/courses/${course_uuid}/`,
        { withCredentials: true }
      )
      .pipe(catchError(this.handleError));
  }
  getCourseSections(course_uuid: string): Observable<any> {
    return this.http
      .get(
        `https://staging.gateway.buri.dev/services/buricourse/courses/${course_uuid}/?include=outline`,
        { withCredentials: true }
      )
      .pipe(catchError(this.handleError));
  }
  getCourseSectionBlocks(section_uuid: string): Observable<any> {
    return this.http
      .get(
        `https://staging.gateway.buri.dev/services/buricourse/sections/${section_uuid}/blocks/`,
        { withCredentials: true }
      )
      .pipe(catchError(this.handleError));
  }
  getCourseSection(section_uuid: string): Observable<any> {
    return this.http
      .get(
        `https:/staging.gateway.buri.dev/services/buricourse/sections/${section_uuid}/`,
        { withCredentials: true }
      )
      .pipe(catchError(this.handleError));
  }
  getBlockList(castID: string, screenID: string): Observable<any> {
    return this.http
      .get(
        `https://staging.gateway.buri.dev/services/buricast/casts/${castID}/screens/${screenID}/blocks`,
        { withCredentials: true }
      )
      .pipe(catchError(this.handleError));
  }
  getBlock(castID: string, screenID: string, blockID: number): Observable<any> {
    return this.http
      .get(
        `https://staging.gateway.buri.dev/services/buricast/casts/${castID}/screens/${screenID}/blocks/${blockID}`,
        { withCredentials: true }
      )
      .pipe(catchError(this.handleError));
  }
  getCast(castID: string): Observable<any> {
    return this.http
      .get(
        `https://staging.gateway.buri.dev/services/buricast/casts/${castID}`,
        {
          withCredentials: true,
        }
      )
      .pipe(catchError(this.handleError));
  }
  getScreenList(castID: string): Observable<any> {
    return this.http
      .get(
        `https://staging.gateway.buri.dev/services/buricast/casts/${castID}/screens`,
        {
          withCredentials: true,
        }
      )
      .pipe(catchError(this.handleError));
  }
  getScreen(castID: string, screenID: string): Observable<any> {
    return this.http
      .get(
        `https://staging.gateway.buri.dev/services/buricast/casts/${castID}/screens/${screenID}`,
        {
          withCredentials: true,
        }
      )
      .pipe(catchError(this.handleError));
  }
  getResourceID(resourceID: string): Observable<any> {
    return this.http
      .get(
        `https://staging.gateway.buri.dev/services/buresource/resources/${resourceID}`,
        {
          withCredentials: true,
        }
      )
      .pipe(catchError(this.handleError));
  }

  getCollection(): Observable<any> {
    return this.http
      .get('https://staging.gateway.buri.dev/services/buresource/collections', {
        withCredentials: true,
      })
      .pipe(catchError(this.handleError));
  }
  getCollect(uuid: string): Observable<any> {
    return this.http
      .get(
        `https://staging.gateway.buri.dev/services/buresource/collections/${uuid}`,
        {
          withCredentials: true,
        }
      )
      .pipe(catchError(this.handleError));
  }
  getCollectionID(collectionID: string): Observable<any> {
    return this.http
      .get(
        `https://staging.gateway.buri.dev/services/buresource/collections/${collectionID}/resources`,
        { withCredentials: true }
      )
      .pipe(catchError(this.handleError));
  }

  getUser(): Observable<any> {
    return this.http
      .get('https://staging.gateway.buri.dev/oauth/userinfo', {
        withCredentials: true,
      })
      .pipe(catchError(this.handleError));
  }
  getSectionList(exam_uuid: string): Observable<any> {
    return this.http
      .get(
        `https://staging.gateway.buri.dev/services/buriexams/v1/exams/${exam_uuid}/sections/`,
        {
          withCredentials: true,
        }
      )
      .pipe(catchError(this.handleError));
  }

  getItems(section_uuid: string): Observable<any> {
    return this.http
      .get(
        `https://staging.gateway.buri.dev/services/buriexams/v1/sections/${section_uuid}/items/`,
        { withCredentials: true }
      )
      .pipe(catchError(this.handleError));
  }
  getItem(item_uuid: string): Observable<any> {
    return this.http
      .get(
        `https://staging.gateway.buri.dev/services/buriexams/v1/items/${item_uuid}/`,
        { withCredentials: true }
      )
      .pipe(catchError(this.handleError));
  }
  getChoices(item_uuid: string): Observable<any> {
    return this.http
      .get(
        `https://staging.gateway.buri.dev/services/buriexams/v1/items/${item_uuid}/choices/`,
        { withCredentials: true }
      )
      .pipe(catchError(this.handleError));
  }
  postBlock(castID: string, screenID: string, block: Block): Observable<any> {
    return this.http
      .post(
        `https://staging.gateway.buri.dev/services/buricast/casts/${castID}/screens/${screenID}/blocks`,
        {
          block: {
            order: block.order,
            type: block.type,
            properties: block.properties,
          },
        },
        { withCredentials: true }
      )
      .pipe(catchError(this.handleError));
  }
  postScreen(castID: string, screen: Screen): Observable<any> {
    return this.http.post(
      `https://staging.gateway.buri.dev/services/buricast/casts/${castID}/screens`,
      {
        screen: {
          title: screen.title,
          description: screen.description,
          slug: screen.slug,
          cast_id: screen.cast_id,
        },
      },
      { withCredentials: true }
    );
  }
  postSection(exam_uuid: string): Observable<any> {
    return this.http
      .post(
        `https://staging.gateway.buri.dev/services/buriexams/v1/exams/${exam_uuid}/sections/`,
        {
          title: 'New Section',
        },
        {
          withCredentials: true,
        }
      )
      .pipe(catchError(this.handleError));
  }
  postItemID(item_uuid_: string, item: Item, order?: number): Observable<any> {
    return this.http
      .post(
        `https://staging.gateway.buri.dev/services/buriexams/v1/items/${item_uuid_}/`,
        {
          explanation: item.explanation,
          hint: item.hint,
          instructions: item.instructions,
          item_uuid: item_uuid_,
          media_url: item.media_url,
          required: item.required,
          shuffle_choices: item.shuffle_choices,
          text: item.text,
          title: item.title,
          type: item.type,
          order_id: order,
          weight: item.weight,
        },
        { withCredentials: true }
      )
      .pipe(catchError(this.handleError));
  }
  postCollection(collect: Collection): Observable<any> {
    return this.http
      .post(
        'https://staging.gateway.buri.dev/services/buresource/collections',
        {
          name: collect.name,
          description: collect.description,
        },
        {
          withCredentials: true,
        }
      )
      .pipe(catchError(this.handleError));
  }

  postItem(section_uuid: string, type_: string): Observable<any> {
    return this.http
      .post(
        `https://staging.gateway.buri.dev/services/buriexams/v1/sections/${section_uuid}/items/`,
        {
          title: 'Question',
          type: type_,
        },
        {
          withCredentials: true,
        }
      )
      .pipe(catchError(this.handleError));
  }
  postChoice(item_uuid: string, choice: Choice): Observable<any> {
    return this.http
      .post(
        `https://staging.gateway.buri.dev/services/buriexams/v1/items/${item_uuid}/choices/`,
        {
          audio_url: choice.audio_url,
          image_url: choice.image_url,
          is_correct: choice.is_correct,
          long_input: choice.long_input,
          order_id: choice.order_id,
          short_input: choice.short_input,
        },
        { withCredentials: true }
      )
      .pipe(catchError(this.handleError));
  }
  postLogOut(): Observable<any> {
    return this.http
      .post(
        'https://staging.gateway.buri.dev/oauth/logout',
        {},
        { withCredentials: true }
      )
      .pipe(catchError(this.handleError));
  }

  postCollectionResource(uuid: string, collectionID: string): Observable<any> {
    return this.http
      .post(
        `https://staging.gateway.buri.dev/services/buresource/collections/${collectionID}/resources`,
        {
          resource_uuids: [uuid],
        },
        {
          withCredentials: true,
        }
      )
      .pipe(catchError(this.handleError));
  }

  postExam(exam: Exam): Observable<any> {
    return this.http
      .post(
        'https://staging.gateway.buri.dev/services/buriexams/v1/exams/',
        {
          title: exam.title,
          description: exam.description,
        },
        { withCredentials: true }
      )
      .pipe(catchError(this.handleError));
  }
  postCourse(course: Course): Observable<any> {
    return this.http
      .post(
        `https://staging.gateway.buri.dev/services/buricourse/courses/`,
        { title: course.title, cover_uri: course.cover_uri, prerequisites: [] },
        { withCredentials: true }
      )
      .pipe(catchError(this.handleError));
  }
  postCourseToResources(
    course_uuid_: string,
    resource: Resource,
    thumbnail: string,
    coll_uuid: string
  ): Observable<any> {
    return this.http
      .post(
        `https://staging.gateway.buri.dev/services/buresource/resources`,
        {
          name: resource.name,
          description: resource.description,
          cover_uri: thumbnail,
          type: resource.type,
          content: {
            course_details: {
              course_uuid: course_uuid_,
            },
            resource_collection_uuid: coll_uuid,
          },
          package_uuids: [coll_uuid],
        },
        { withCredentials: true }
      )
      .pipe(catchError(this.handleError));
  }
  postCourseSection(course_uuid: string, section: Section): Observable<any> {
    return this.http
      .post(
        `https://staging.gateway.buri.dev/services/buricourse/courses/${course_uuid}/sections/`,
        { title: section.title, order_id: section.order_id, prerequisites: [] },
        { withCredentials: true }
      )
      .pipe(catchError(this.handleError));
  }
  postExamToResources(
    exam_uuid: string,
    exam_timeslot: string,
    resource: Resource,
    thumbnail: string
  ): Observable<any> {
    return this.http
      .post(
        'https://staging.gateway.buri.dev/services/buresource/resources',
        {
          name: resource.name,
          description: resource.description,
          cover_uri: thumbnail,
          type: resource.type,
          content: {
            exam_details: {
              exam_uuid: exam_uuid,
              exam_timeslot_uuid: exam_timeslot,
            },
          },
        },
        { withCredentials: true }
      )
      .pipe(catchError(this.handleError));
  }

  postTimeSlot(exam_uuid: string): Observable<any> {
    return this.http
      .post(
        `https://staging.gateway.buri.dev/services/buriexams/v1/exams/${exam_uuid}/timeslots/`,
        { access_type: 'PU' },
        { withCredentials: true }
      )
      .pipe(catchError(this.handleError));
  }
  postAuth(paramsCode: string): Observable<any> {
    return this.http
      .post(
        'https://staging.gateway.buri.dev/oauth',
        {
          code: paramsCode,
          redirect_uri: environment.prod_uri,
          client_id:
            '3dfd73be42760397af42e5a56fb87331690f89eb9201551184f538063938a912',
        },
        {
          withCredentials: true,
        }
      )
      .pipe(catchError(this.handleError));
  }

  postResource(resource: Resource): Observable<any> {
    return this.http
      .post(
        'https://staging.gateway.buri.dev/services/buresource/resources',
        {
          name: resource.name,
          description: resource.description,
          type: resource.type,
          content: { uri: resource.content },
        },
        {
          withCredentials: true,
        }
      )
      .pipe(catchError(this.handleError));
  }
  postCourseBlock(section_uuid: string, sec_block: SecBlock): Observable<any> {
    return this.http
      .post(
        `https://staging.gateway.buri.dev/services/buricourse/sections/${section_uuid}/blocks/`,
        {
          title: sec_block.title,
          cover_uri: sec_block.cover_uri,
          content: {
            resource_details: sec_block.content,
          },
        },
        { withCredentials: true }
      )
      .pipe(catchError(this.handleError));
  }
  postResourceWithThumbnail(
    resource: Resource,
    thumbnail: string,
    contentType: string
  ): Observable<any> {
    return this.http
      .post(
        'https://staging.gateway.buri.dev/services/buresource/resources',
        {
          name: resource.name,
          description: resource.description,
          cover_uri: thumbnail,
          type: resource.type,
          content: { uri: resource.content, content_type: contentType },
        },
        {
          withCredentials: true,
        }
      )
      .pipe(catchError(this.handleError));
  }

  postFile(file_: any): Observable<any> {
    return this.http
      .post(
        'https://staging.gateway.buri.dev/services/buriuploads/uploads',
        file_,
        {
          withCredentials: true,
        }
      )
      .pipe(catchError(this.handleError));
  }
  updateCourseInResources(
    course_uuid_: string,
    resId: string,
    resource: Resource,
    thumbnail: string,
    coll_uuid: string
  ): Observable<any> {
    return this.http
      .put(
        `https://staging.gateway.buri.dev/services/buresource/resources/${resId}`,
        {
          name: resource.name,
          description: resource.description,
          cover_uri: thumbnail,
          type: resource.type,
          content: {
            course_details: {
              course_uuid: course_uuid_,
            },
            resource_collection_uuid: coll_uuid,
          },
          package_uuids: [coll_uuid],
        },
        { withCredentials: true }
      )
      .pipe(catchError(this.handleError));
  }
  updateCourse(course_uuid: string, course: Course): Observable<any> {
    return this.http
      .post(
        `https://staging.gateway.buri.dev/services/buricourse/courses/${course_uuid}/`,
        { title: course.title, cover_uri: course.cover_uri },
        { withCredentials: true }
      )
      .pipe(catchError(this.handleError));
  }
  updateCourseSection(section_uuid: string, section: Section): Observable<any> {
    return this.http
      .post(
        `https://staging.gateway.buri.dev/services/buricourse/sections/${section_uuid}/`,
        { title: section.title },
        { withCredentials: true }
      )
      .pipe(catchError(this.handleError));
  }
  updateCourseSectionOrder(
    section_uuid: string,
    order: string
  ): Observable<any> {
    return this.http
      .post(
        `https://staging.gateway.buri.dev/services/buricourse/sections/${section_uuid}/`,
        { order_id: order },
        { withCredentials: true }
      )
      .pipe(catchError(this.handleError));
  }
  updateCourseBlockOrder(
    sec_uuid: string,
    block_uuid: string,
    order: string
  ): Observable<any> {
    return this.http
      .post(
        `https://staging.gateway.buri.dev/services/buricourse/blocks/${block_uuid}/`,
        { section: sec_uuid, order_id: order },
        { withCredentials: true }
      )
      .pipe(catchError(this.handleError));
  }
  updateChoice(choice_uuid: string, choice: Choice): Observable<any> {
    return this.http
      .post(
        `https://staging.gateway.buri.dev/services/buriexams/v1/choices/${choice_uuid}/`,
        {
          audio_url: choice.audio_url,
          image_url: choice.image_url,
          is_correct: choice.is_correct,
          long_input: choice.long_input,
          order_id: choice.order_id,
          short_input: choice.short_input,
        },
        { withCredentials: true }
      )
      .pipe(catchError(this.handleError));
  }
  updateExamToResources(
    exam_uuid: string,
    exam_timeslot: string,
    resource: Resource,
    thumbnail: string,
    res_uuid: string
  ): Observable<any> {
    return this.http
      .put(
        `https://staging.gateway.buri.dev/services/buresource/resources/${res_uuid}`,
        {
          name: resource.name,
          description: resource.description,
          cover_uri: thumbnail,
          type: resource.type,
          content: {
            exam_details: {
              exam_uuid: exam_uuid,
              exam_timeslot_uuid: exam_timeslot,
            },
          },
        },
        { withCredentials: true }
      )
      .pipe(catchError(this.handleError));
  }
  updateBlock(
    castID: string,
    screenID: string,
    blockID: number,
    block: Block
  ): Observable<any> {
    return this.http
      .patch(
        `https://staging.gateway.buri.dev/services/buricast/casts/${castID}/screens/${screenID}/blocks/${blockID}`,
        {
          block: {
            order: block.order,
            type: block.type,
            properties: block.properties,
          },
        },
        { withCredentials: true }
      )
      .pipe(catchError(this.handleError));
  }

  updateCollection(uuid: string, collection: Collection): Observable<any> {
    return this.http
      .put(
        `https://staging.gateway.buri.dev/services/buresource/collections/${uuid}`,
        { name: collection.name },
        { withCredentials: true }
      )
      .pipe(catchError(this.handleError));
  }
  updateResource(resource: Resource, resourceID: string): Observable<any> {
    return this.http.put(
      `https://staging.gateway.buri.dev/services/buresource/resources/${resourceID}`,
      {
        name: resource.name,
        description: resource.description,
        type: resource.type,
        content: { uri: resource.content },
      },
      {
        withCredentials: true,
      }
    );
  }

  updateResourceWithThumbnail(
    resource: Resource,
    resourceID: string,
    thumbnail: string,
    contentType: string
  ): Observable<any> {
    return this.http.put(
      `https://staging.gateway.buri.dev/services/buresource/resources/${resourceID}`,
      {
        name: resource.name,
        description: resource.description,
        cover_uri: thumbnail,
        type: resource.type,
        content: { uri: resource.content, content_type: contentType },
      },
      {
        withCredentials: true,
      }
    );
  }
  updateScreen(
    castID: string,
    screenID: string,
    title: string
  ): Observable<any> {
    return this.http.patch(
      `https://staging.gateway.buri.dev/services/buricast/casts/${castID}/screens/${screenID}`,
      {
        screen: {
          title: title,
        },
      },
      { withCredentials: true }
    );
  }
  deleteCourseSection(section_uuid: string): Observable<any> {
    return this.http
      .delete(
        `https://staging.gateway.buri.dev/services/buricourse/sections/${section_uuid}/`,
        { withCredentials: true }
      )
      .pipe(catchError(this.handleError));
  }
  deleteCourseSectionBlock(block_uuid: string): Observable<any> {
    return this.http
      .delete(
        `https://staging.gateway.buri.dev/services/buricourse/blocks/${block_uuid}/`,
        { withCredentials: true }
      )
      .pipe(catchError(this.handleError));
  }
  deleteChoice(choice_uuid: string): Observable<any> {
    return this.http
      .delete(
        `https://staging.gateway.buri.dev/services/buriexams/v1/choices/${choice_uuid}/`,
        { withCredentials: true }
      )
      .pipe(catchError(this.handleError));
  }
  deleteExam(exam_uuid: string): Observable<any> {
    return this.http
      .delete(
        `https://staging.gateway.buri.dev/services/buriexams/v1/exams/${exam_uuid}/`,
        { withCredentials: true }
      )
      .pipe(catchError(this.handleError));
  }
  deleteItem(item_uuid: string): Observable<any> {
    return this.http
      .delete(
        `https://staging.gateway.buri.dev/services/buriexams/v1/items/${item_uuid}/`,
        { withCredentials: true }
      )
      .pipe(catchError(this.handleError));
  }
  deleteResource(resourceID: string) {
    return this.http.delete(
      `https://staging.gateway.buri.dev/services/buresource/resources/${resourceID}`,
      {
        withCredentials: true,
      }
    );
  }
  deleteBlock(
    castID: string,
    screenID: string,
    blockID: number
  ): Observable<any> {
    return this.http
      .delete(
        `https://staging.gateway.buri.dev/services/buricast/casts/${castID}/screens/${screenID}/blocks/${blockID}`,
        { withCredentials: true }
      )
      .pipe(catchError(this.handleError));
  }
  deleteScreen(castID: string, screenID: string): Observable<any> {
    return this.http
      .delete(
        `https://staging.gateway.buri.dev/services/buricast/casts/${castID}/screens/${screenID}`,
        {
          withCredentials: true,
        }
      )
      .pipe(catchError(this.handleError));
  }

  handleError(error: HttpErrorResponse) {
    if (error.status === 0) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, body was: `,
        error.error
      );
    }
    // Return an observable with a user-facing error message.
    return throwError('Something bad happened; please try again later.');
  }
}
