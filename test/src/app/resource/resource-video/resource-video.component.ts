import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { mergeMap } from 'rxjs/operators';
import { HttpService } from 'src/app/http.service';
import { SecBlock } from 'src/app/section-block';
import { Resource } from 'src/resource';

@Component({
  selector: 'app-resource-video',
  templateUrl: './resource-video.component.html',
  styleUrls: ['./resource-video.component.css'],
})
export class ResourceVideoComponent implements OnInit {
  constructor(
    private routerLink: Router,
    private httpService: HttpService,
    private route: ActivatedRoute
  ) {}
  uuid: string = '';
  screenID: string = '';
  fileName: string = '';
  submittedVideo: boolean = false;
  submittedImg: boolean = false;
  submitted: boolean = false;
  imgUri: string = '';
  requiredFileType: string = 'video/*';
  requiredImg: string = 'img/jpeg, img/png, img/gif';
  video = new Resource('', '', 'video', '');
  videoType: string = '';
  placeholder: string = './assets/img/Placeholder.png';
  uploading: boolean = false;
  uploadingImg: boolean = false;
  block = new SecBlock('', '', {});
  inside_course: boolean = false;
  resourceID: string = '';
  sec_uuid: string = '';
  courseID: string = '';
  ngOnInit(): void {
    this.route.queryParams.subscribe((params) => {
      this.uuid = params.uuid;
      this.screenID = params.screenId;
      this.inside_course = params.is_inside_course;
      this.resourceID = params.resId;
      this.sec_uuid = params.secId;
      this.courseID = params.course_uuid;
    });
  }

  onFileSelected(event: any) {
    const file: File = event.target.files[0];
    if (file) {
      const formData = new FormData();

      formData.append('file', file);
      if (file.type == 'video/mp4') {
        this.uploading = true;
        this.videoType = file.type;
        this.fileName = file.name;
        this.httpService.postFile(formData).subscribe((response) => {
          this.video.content = response.uri;
          this.submittedVideo = true;
          this.uploading = false;
        });
      }
      if (
        file.type == 'image/gif' ||
        file.type == 'image/jpeg' ||
        file.type == 'image/png'
      ) {
        this.uploadingImg = true;
        this.httpService.postFile(formData).subscribe((response) => {
          this.imgUri = response.uri;
          this.submittedImg = true;
          this.uploadingImg = false;
        });
      }
    }
  }
  addResource() {
    this.submitted = true;
    if (this.inside_course) {
      this.httpService
        .postResourceWithThumbnail(
          this.video,
          this.imgUri,
          this.requiredFileType
        )
        .pipe(
          mergeMap((response) => {
            delete response.content;
            console.log(response);
            this.block.title = this.video.name;
            this.block.cover_uri = this.imgUri;
            this.block.content = response;
            return this.httpService.postCourseBlock(this.sec_uuid, this.block);
          })
        )
        .subscribe(() => {
          this.routerLink.navigate(['/course-content'], {
            queryParams: {
              screenId: this.screenID,
              course_uuid: this.courseID,
              resId: this.resourceID,
              uuid: this.uuid,
            },
          });
        });
    } else {
      this.httpService
        .postResourceWithThumbnail(
          this.video,
          this.imgUri,
          this.requiredFileType
        )
        .pipe(
          mergeMap((response) => {
            return this.httpService.postCollectionResource(
              response.uuid,
              this.uuid
            );
          })
        )
        .subscribe(() => {
          if (this.screenID) {
            this.routerLink.navigate(['/cast-editor'], {
              queryParams: { screenId: this.screenID },
            });
          } else {
            this.routerLink.navigate(['/resources'], {
              queryParams: { uuid: this.uuid },
            });
          }
        });
    }
  }
}
