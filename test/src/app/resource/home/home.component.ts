import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpService } from 'src/app/http.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  jsonValue = { firstname: '', lastname: '' };

  constructor(private httpService: HttpService, private routerLink: Router) {}

  ngOnInit(): void {
    this.httpService.getUser().subscribe((name) => {
      this.jsonValue.firstname = name.first_name;
      this.jsonValue.lastname = name.last_name;
    });
  }

  profile() {
    this.routerLink.navigateByUrl('/home');
  }

  continue() {
    this.routerLink.navigateByUrl('/collections');
  }
}
