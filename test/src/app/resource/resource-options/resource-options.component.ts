import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpService } from 'src/app/http.service';

@Component({
  selector: 'app-resource-options',
  templateUrl: './resource-options.component.html',
  styleUrls: ['./resource-options.component.css'],
})
export class ResourceOptionsComponent implements OnInit {
  constructor(
    private routerLink: Router,
    private httpService: HttpService,
    private route: ActivatedRoute
  ) {}
  submitted = false;
  uuid: string = '';
  screenID: string = '';
  resourceID: string = '';
  sectionID: string = '';
  courseID: string = '';
  inside_course: boolean = false;
  ngOnInit(): void {
    this.route.queryParams.subscribe((params) => {
      this.uuid = params.uuid;
      this.screenID = params.screenId;
      this.inside_course = params.is_inside_course;
      this.sectionID = params.secId;
      this.resourceID = params.resId;
      this.courseID = params.course_uuid;
    });
  }

  addLink() {
    if (this.screenID && !this.inside_course) {
      this.routerLink.navigate(['/resource-link'], {
        queryParams: { screenId: this.screenID, uuid: this.uuid },
      });
    } else if (this.inside_course) {
      this.routerLink.navigate(['/resource-link'], {
        queryParams: {
          is_inside_course: true,
          course_uuid: this.courseID,
          secId: this.sectionID,
          resId: this.resourceID,
          uuid: this.uuid,
          screenId: this.screenID,
        },
      });
    } else {
      this.routerLink.navigate(['/resource-link'], {
        queryParams: { uuid: this.uuid },
      });
    }
  }
  addPDF() {
    if (this.screenID && !this.inside_course) {
      this.routerLink.navigate(['/resource-pdf'], {
        queryParams: { screenId: this.screenID, uuid: this.uuid },
      });
    } else if (this.inside_course) {
      this.routerLink.navigate(['/resource-pdf'], {
        queryParams: {
          is_inside_course: true,
          course_uuid: this.courseID,
          secId: this.sectionID,
          resId: this.resourceID,
          uuid: this.uuid,
          screenId: this.screenID,
        },
      });
    } else {
      this.routerLink.navigate(['/resource-pdf'], {
        queryParams: { uuid: this.uuid },
      });
    }
  }
  addVideo() {
    if (this.screenID && !this.inside_course) {
      this.routerLink.navigate(['/resource-video'], {
        queryParams: { screenId: this.screenID, uuid: this.uuid },
      });
    } else if (this.inside_course) {
      this.routerLink.navigate(['/resource-video'], {
        queryParams: {
          is_inside_course: true,
          course_uuid: this.courseID,
          secId: this.sectionID,
          resId: this.resourceID,
          uuid: this.uuid,
          screenId: this.screenID,
        },
      });
    } else {
      this.routerLink.navigate(['/resource-video'], {
        queryParams: { uuid: this.uuid },
      });
    }
  }

  addExam() {
    if (this.screenID && !this.inside_course) {
      this.routerLink.navigate(['/create-exam'], {
        queryParams: { screenId: this.screenID, uuid: this.uuid },
      });
    } else if (this.inside_course) {
      this.routerLink.navigate(['/create-exam'], {
        queryParams: {
          is_inside_course: true,
          course_uuid: this.courseID,
          secId: this.sectionID,
          resId: this.resourceID,
          uuid: this.uuid,
          screenId: this.screenID,
        },
      });
    } else {
      this.routerLink.navigate(['/create-exam'], {
        queryParams: { uuid: this.uuid },
      });
    }
  }

  addCourse() {
    if (this.screenID && !this.inside_course) {
      this.routerLink.navigate(['/create-course'], {
        queryParams: { screenId: this.screenID, uuid: this.uuid },
      });
    } else {
      this.routerLink.navigate(['/create-course'], {
        queryParams: { uuid: this.uuid },
      });
    }
  }
}
