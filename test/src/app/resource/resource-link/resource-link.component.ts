import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { mergeMap } from 'rxjs/operators';
import { HttpService } from 'src/app/http.service';
import { SecBlock } from 'src/app/section-block';
import { Resource } from 'src/resource';

@Component({
  selector: 'app-resource-link',
  templateUrl: './resource-link.component.html',
  styleUrls: ['./resource-link.component.css'],
})
export class ResourceLinkComponent implements OnInit {
  constructor(
    private routerLink: Router,
    private httpService: HttpService,
    private route: ActivatedRoute
  ) {}

  collectID: string = '';
  screenID: string = '';
  inside_course: boolean = false;
  sec_uuid: string = '';
  resourceID: string = '';
  courseID: string = '';

  ngOnInit(): void {
    this.route.queryParams.subscribe((params) => {
      this.collectID = params.uuid;
      this.screenID = params.screenId;
      this.inside_course = params.is_inside_course;
      this.resourceID = params.resId;
      this.sec_uuid = params.secId;
      this.courseID = params.course_uuid;
    });
  }
  submitted = false;

  link = new Resource('', '', 'link', '');
  block = new SecBlock('', '', {});
  addResource() {
    this.submitted = true;
    if (this.inside_course) {
      this.httpService
        .postResource(this.link)
        .pipe(
          mergeMap((response) => {
            delete response.content;
            console.log(response);
            this.block.title = this.link.name;
            this.block.cover_uri = '';
            this.block.content = response;
            return this.httpService.postCourseBlock(this.sec_uuid, this.block);
          })
        )
        .subscribe(() => {
          this.routerLink.navigate(['/course-content'], {
            queryParams: {
              screenId: this.screenID,
              course_uuid: this.courseID,
              resId: this.resourceID,
              uuid: this.collectID,
            },
          });
        });
    } else {
      this.httpService
        .postResource(this.link)
        .pipe(
          mergeMap((response) => {
            return this.httpService.postCollectionResource(
              response.uuid,
              this.collectID
            );
          })
        )
        .subscribe(() => {
          if (this.screenID) {
            this.routerLink.navigate(['/cast-editor'], {
              queryParams: {
                screenId: this.screenID,
              },
            });
          } else {
            this.routerLink.navigate(['/resources'], {
              queryParams: { uuid: this.collectID },
            });
          }
        });
    }
  }
}
