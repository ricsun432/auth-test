import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CollectionFormComponent } from './collection-form/collection-form.component';
import { CollectionsComponent } from './collections/collections.component';
import { EditLinkComponent } from './edit-link/edit-link.component';
import { EditPdfComponent } from './edit-pdf/edit-pdf.component';
import { EditVideoComponent } from './edit-video/edit-video.component';
import { HomeComponent } from './home/home.component';
import { ResourceLinkComponent } from './resource-link/resource-link.component';
import { ResourceOptionsComponent } from './resource-options/resource-options.component';
import { ResourcePdfComponent } from './resource-pdf/resource-pdf.component';
import { ResourceVideoComponent } from './resource-video/resource-video.component';
import { ResourceRoutingModule } from './resource.routing';
import { ResourcesComponent } from './resources/resources.component';

@NgModule({
  declarations: [
    ResourceVideoComponent,
    CollectionFormComponent,
    CollectionsComponent,
    EditLinkComponent,
    EditPdfComponent,
    EditVideoComponent,
    HomeComponent,
    ResourceLinkComponent,
    ResourceOptionsComponent,
    ResourcePdfComponent,
    ResourceVideoComponent,
    ResourcesComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ResourceRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
  ],
})
export class ResourceModule {}
