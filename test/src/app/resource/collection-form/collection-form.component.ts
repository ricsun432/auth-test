import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Collection } from 'src/app/collection';
import { HttpService } from 'src/app/http.service';

@Component({
  selector: 'app-collection-form',
  templateUrl: './collection-form.component.html',
  styleUrls: ['./collection-form.component.css'],
})
export class CollectionFormComponent implements OnInit {
  constructor(private httpService: HttpService, private routerLink: Router) {}

  ngOnInit(): void {}
  collect = new Collection('', '');
  submitted = false;

  addCollection() {
    this.httpService
      .postCollection(this.collect)
      .subscribe(() => this.routerLink.navigateByUrl('/collections'));
  }

  onSubmit() {
    this.submitted = true;
  }
}
