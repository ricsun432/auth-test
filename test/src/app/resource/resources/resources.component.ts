import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { mergeMap } from 'rxjs/operators';
import { HttpService } from 'src/app/http.service';

@Component({
  selector: 'app-resources',
  templateUrl: './resources.component.html',
  styleUrls: ['./resources.component.css'],
})
export class ResourcesComponent implements OnInit {
  uuid: string = '';
  options = ['Add Resource', 'Edit Resource', 'Delete Resource'];
  list: any = [];
  search: string = '';
  constructor(
    private route: ActivatedRoute,
    private routerLink: Router,
    private httpService: HttpService
  ) {}

  ngOnInit(): void {
    this.route.queryParams
      .pipe(
        mergeMap((params) => {
          this.uuid = params.uuid;
          return this.httpService.getCollectionID(this.uuid);
        })
      )
      .subscribe((response) => {
        this.list = response;
      });
  }
  addResource() {
    this.routerLink.navigate(['/resource-options'], {
      queryParams: { uuid: this.uuid },
    });
  }

  openResource(index: number) {
    if (this.list[index].type == 'pdf')
      this.routerLink.navigate(['/edit-pdf'], {
        queryParams: { uuid: this.list[index].uuid, collid: this.uuid },
      });
    if (this.list[index].type == 'video')
      this.routerLink.navigate(['/edit-video'], {
        queryParams: { uuid: this.list[index].uuid, collid: this.uuid },
      });
    if (this.list[index].type == 'link')
      this.routerLink.navigate(['/edit-link'], {
        queryParams: { uuid: this.list[index].uuid, collid: this.uuid },
      });
  }

  goToCollection() {
    this.routerLink.navigateByUrl('/collections');
  }
}
