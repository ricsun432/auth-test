import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpService } from 'src/app/http.service';

@Component({
  selector: 'app-collections',
  templateUrl: './collections.component.html',
  styleUrls: ['./collections.component.css'],
})
export class CollectionsComponent implements OnInit {
  constructor(
    private router: ActivatedRoute,
    private routerLink: Router,
    private httpService: HttpService
  ) {}

  list: any = [];
  search: string = '';

  ngOnInit(): void {
    this.httpService.getCollection().subscribe((collection) => {
      this.list = collection;
    });
  }

  collectionForm() {
    this.routerLink.navigateByUrl('/collection-form');
  }
  resourceList(id: string) {
    this.routerLink.navigate(['/resources'], { queryParams: { uuid: id } });
  }
}
