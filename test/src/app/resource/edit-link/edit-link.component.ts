import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { mergeMap } from 'rxjs/operators';
import { HttpService } from 'src/app/http.service';
import { Resource } from 'src/resource';

@Component({
  selector: 'app-edit-link',
  templateUrl: './edit-link.component.html',
  styleUrls: ['./edit-link.component.css'],
})
export class EditLinkComponent implements OnInit {
  constructor(
    private httpService: HttpService,
    private routerLink: Router,
    private route: ActivatedRoute
  ) {}
  collectID: string = '';
  resourceID: string = '';
  screenID: string = '';
  imgUri: string = '';

  submittedPDF: boolean = false;
  submittedImg: boolean = false;

  list: any = {};
  link = new Resource('', '', 'link', '');
  videoType: string = '';
  ngOnInit(): void {
    this.route.queryParams
      .pipe(
        mergeMap((params) => {
          this.collectID = params.collid;
          this.resourceID = params.uuid;
          this.screenID = params.screenId;
          return this.httpService.getResourceID(this.resourceID);
        })
      )
      .subscribe((response) => {
        this.list = response;
        this.imgUri = response.cover_uri;
        this.link.content = response.content.uri;
        this.link.name = response.name;
        this.link.description = response.description;
      });
  }
  updateResource() {
    this.httpService
      .updateResource(this.link, this.resourceID)
      .subscribe(() => {
        if (this.screenID) {
          this.routerLink.navigate(['/cast-editor'], {
            queryParams: { screenId: this.screenID },
          });
        } else {
          this.routerLink.navigate(['/resources'], {
            queryParams: { uuid: this.collectID },
          });
        }
      });
  }
  deleteResource() {
    this.httpService.deleteResource(this.resourceID).subscribe(() => {
      if (this.screenID) {
        this.routerLink.navigate(['/cast-editor'], {
          queryParams: { screenId: this.screenID },
        });
      } else {
        this.routerLink.navigate(['/resources'], {
          queryParams: { uuid: this.collectID },
        });
      }
    });
  }
}
