import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResourcePdfComponent } from './resource-pdf.component';

describe('ResourcePdfComponent', () => {
  let component: ResourcePdfComponent;
  let fixture: ComponentFixture<ResourcePdfComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResourcePdfComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResourcePdfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
