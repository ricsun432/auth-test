import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CollectionFormComponent } from './collection-form/collection-form.component';
import { CollectionsComponent } from './collections/collections.component';
import { EditLinkComponent } from './edit-link/edit-link.component';
import { EditPdfComponent } from './edit-pdf/edit-pdf.component';
import { EditVideoComponent } from './edit-video/edit-video.component';
import { HomeComponent } from './home/home.component';
import { ResourceLinkComponent } from './resource-link/resource-link.component';
import { ResourceOptionsComponent } from './resource-options/resource-options.component';
import { ResourcePdfComponent } from './resource-pdf/resource-pdf.component';
import { ResourceVideoComponent } from './resource-video/resource-video.component';
import { ResourcesComponent } from './resources/resources.component';

const routes: Routes = [
  { path: 'resource-video', component: ResourceVideoComponent },
  { path: 'resource-pdf', component: ResourcePdfComponent },
  { path: 'resource-link', component: ResourceLinkComponent },
  { path: 'resources', component: ResourcesComponent },
  { path: 'collection-form', component: CollectionFormComponent },
  { path: 'collections', component: CollectionsComponent },
  { path: 'edit-link', component: EditLinkComponent },
  { path: 'edit-pdf', component: EditPdfComponent },
  { path: 'edit-video', component: EditVideoComponent },
  { path: 'home', component: HomeComponent },
  { path: 'resource-options', component: ResourceOptionsComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ResourceRoutingModule {}
