import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { mergeMap } from 'rxjs/operators';
import { HttpService } from 'src/app/http.service';
import { Resource } from 'src/resource';

@Component({
  selector: 'app-edit-video',
  templateUrl: './edit-video.component.html',
  styleUrls: ['./edit-video.component.css'],
})
export class EditVideoComponent implements OnInit {
  placeholder: string = './assets/img/Placeholder.png';
  collectID: string = '';
  resourceID: string = '';
  screenID: string = '';
  imgUri: string = '';
  submittedVideo: boolean;
  submittedImg: boolean;
  fileName: string = '';
  requiredFileType: string = 'video/*';
  requiredImg: string = 'image/png, image/jpeg, image/gif';
  list: any = {};
  video = new Resource('', '', 'video', '');
  videoType: string = '';
  uploading: boolean = false;
  uploadingImg: boolean = false;
  constructor(
    private httpService: HttpService,
    private routerLink: Router,
    private route: ActivatedRoute
  ) {
    this.submittedVideo = false;
    this.submittedImg = false;
  }

  ngOnInit(): void {
    this.route.queryParams
      .pipe(
        mergeMap((params) => {
          this.collectID = params.collid;
          this.resourceID = params.uuid;
          this.screenID = params.screenId;

          return this.httpService.getResourceID(this.resourceID);
        })
      )
      .subscribe((response) => {
        this.list = response;
        this.imgUri = response.cover_uri;
        this.video.content = response.content.uri;
        this.video.name = response.name;
        this.video.description = response.description;
      });
  }
  onFileSelected(event: any) {
    const file: File = event.target.files[0];
    if (file) {
      const formData = new FormData();

      formData.append('file', file);
      if (file.type == 'video/mp4') {
        this.uploading = true;
        this.videoType = file.type;
        this.fileName = file.name;
        this.httpService.postFile(formData).subscribe((response) => {
          this.video.content = response.uri;
          this.submittedVideo = !this.submittedVideo;
          this.uploading = false;
        });
      }
      if (
        file.type == 'image/gif' ||
        file.type == 'image/jpeg' ||
        file.type == 'image/png'
      ) {
        this.uploadingImg = true;
        this.httpService.postFile(formData).subscribe((response) => {
          this.imgUri = response.uri;
          this.submittedImg = true;
          this.uploadingImg = false;
        });
      }
    }
  }
  updateResource() {
    this.httpService
      .updateResourceWithThumbnail(
        this.video,
        this.resourceID,
        this.imgUri,
        this.requiredFileType
      )
      .subscribe(() => {
        if (this.screenID) {
          this.routerLink.navigate(['/cast-editor'], {
            queryParams: { screenId: this.screenID },
          });
        } else {
          this.routerLink.navigate(['/resources'], {
            queryParams: { uuid: this.collectID },
          });
        }
      });
  }
  deleteResource() {
    this.httpService.deleteResource(this.resourceID).subscribe(() => {
      if (this.screenID) {
        this.routerLink.navigate(['/cast-editor'], {
          queryParams: { screenId: this.screenID },
        });
      } else {
        this.routerLink.navigate(['/resources'], {
          queryParams: { uuid: this.collectID },
        });
      }
    });
  }
}
