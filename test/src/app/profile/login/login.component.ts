import { Component, OnInit } from '@angular/core';
import { NavbarService } from 'src/app/navbar.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  constructor(public nav: NavbarService) {}

  ngOnInit(): void {
    this.nav.hide();
  }

  login() {
    location.href = `https://staging.pass.buri.dev/oauth/authorize?client_id=3dfd73be42760397af42e5a56fb87331690f89eb9201551184f538063938a912&redirect_uri=${environment.prod_uri}&response_type=code&scope=openid+profile+email`;
  }
}
