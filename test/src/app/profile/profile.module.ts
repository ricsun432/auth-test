import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AuthComponent } from './auth/auth.component';
import { LoginComponent } from './login/login.component';
import { ProfileRoutingModule } from './profile.routing';

@NgModule({
  declarations: [AuthComponent, LoginComponent],
  imports: [CommonModule, ProfileRoutingModule],
})
export class ProfileModule {}
