import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpService } from 'src/app/http.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css'],
})
export class AuthComponent implements OnInit {
  constructor(
    private router: ActivatedRoute,
    private routerLink: Router,
    private httpService: HttpService
  ) {}

  ngOnInit(): void {
    this.router.queryParams.subscribe((params) => {
      this.httpService
        .postAuth(params.code)
        .subscribe(() => this.routerLink.navigate(['/home']));
    });
  }
}
