export class Course {
  constructor(
    public title: string,
    public cover_uri: string,
    public prerequisites?: any
  ) {}
}
