export class Section {
  constructor(
    public title: string,
    public order_id: string,
    public prerequisites?: any
  ) {}
}
