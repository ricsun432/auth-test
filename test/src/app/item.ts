export class Item {
  constructor(
    public text: string,
    public title: string,
    public type: string,
    public required: boolean,
    public weight?: string,
    public media_url?: string,
    public explanation?: string,
    public hint?: string,
    public instructions?: string,
    public show_results?: boolean,
    public shuffle_choices?: boolean,
    public order_id?: number
  ) {}
}
