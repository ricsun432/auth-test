import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { mergeMap } from 'rxjs/operators';
import { Choice } from 'src/app/choice';
import { HttpService } from 'src/app/http.service';
import { Item } from 'src/app/item';

@Component({
  selector: 'app-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.css'],
})
export class CheckboxComponent implements OnInit {
  constructor(
    private routerLink: Router,
    private route: ActivatedRoute,
    private httpService: HttpService
  ) {}

  exam_uuid: string = '';
  resourceID: string = '';
  item_uuid: string = '';
  coll_uuid: string = '';
  screenID: string = '';
  section_uuid: string = '';
  requiredImg: string = 'image/png, image/jpeg, image/gif';
  uploadingImg: boolean = false;
  imgUri: string = '';
  submittedImg: boolean = false;
  item = new Item(
    '',
    'Question',
    'CB',
    true,
    '1.00',
    '',
    '',
    '',
    '',
    false,
    false
  );
  choice = new Choice('', '', false, '', 0, '');
  options: any = [];
  list: any = [];

  picIndex: number = 0;
  clicked: boolean = false;
  index: number = 0;
  input: string = '';
  correct: boolean = false;
  posted: boolean = false;
  uploading: any = [];
  inputTxt: any = [];
  newImg: boolean = false;
  inside_course: boolean = false;
  crs_uuid: string = '';
  ngOnInit(): void {
    this.route.queryParams
      .pipe(
        mergeMap((params) => {
          this.inside_course = params.is_inside_course;
          this.crs_uuid = params.course_uuid;
          this.item_uuid = params.itemId;
          this.coll_uuid = params.uuid;
          this.screenID = params.screenId;
          this.section_uuid = params.secId;
          this.resourceID = params.resId;
          this.exam_uuid = params.examId;
          return this.httpService.getItem(this.item_uuid);
        }),
        mergeMap((response) => {
          this.item.media_url = response.media_url;
          this.item.required = response.required;
          console.log(response);
          this.item.text = response.text;
          return this.httpService.getChoices(this.item_uuid);
        })
      )
      .subscribe((options) => {
        console.log(options);
        this.options = options;
        for (let option of options) {
          let choice = new Choice('', '', false, '', 0, '');

          choice.audio_url = option.audio_url;
          choice.image_url = option.image_url;
          choice.is_correct = option.is_correct;
          choice.long_input = option.long_input;
          choice.order_id = option.order_id;
          choice.short_input = option.short_input;
          this.inputTxt.push(option.short_input);
          this.list.push(choice);
        }
        if (this.list.length > 0) {
          this.index = this.list.length;
        } else {
          this.index = 0;
        }
        console.log(options);
      });
  }

  onFileSelected(event: any) {
    const file: File = event.target.files[0];
    if (file) {
      const formData = new FormData();

      formData.append('file', file);
      if (
        file.type == 'image/gif' ||
        file.type == 'image/jpeg' ||
        file.type == 'image/png'
      ) {
        if (this.clicked) {
          this.uploading[this.picIndex] = true;
        } else {
          this.uploadingImg = true;
        }
        this.httpService.postFile(formData).subscribe((response) => {
          if (this.clicked) {
            if (this.newImg) {
              this.onEnter();
              this.list[this.picIndex].image_url = response.uri;
              this.clicked = false;
              this.uploading[this.picIndex] = false;
            } else {
              this.list[this.picIndex].image_url = response.uri;
              this.clicked = false;
              this.uploading[this.picIndex] = false;
            }
          } else {
            this.imgUri = response.uri;
            this.item.media_url = response.uri;
            this.uploadingImg = false;
          }

          this.submittedImg = true;
        });
      }
    }
  }
  onChange() {
    console.log(this.item.required);
  }

  onEnter() {
    if (this.input === '' && !this.uploading[this.picIndex]) {
      return 1;
    }
    let choice = new Choice('', '', false, '', 0, '');
    if (this.index === 0) choice.is_correct = true;
    choice.short_input = this.input;
    choice.long_input = this.input;
    choice.order_id = ++this.index;
    this.list.push(choice);
    this.inputTxt.push(this.input);
    console.log(this.list);
    this.input = '';
    return 0;
  }

  deleteChoice(index: number) {
    if (this.options.length === this.list.length) {
      console.log(this.options.length);
      this.httpService.deleteChoice(this.options[index].uuid).subscribe(() => {
        this.options.splice(index, 1);
        this.inputTxt.splice(index, 1);
        this.list.splice(index, 1);
        this.index--;
        for (let i = index; i < this.list.length; i++) {
          this.list[i].order_id--;
        }
      });
    } else {
      this.inputTxt.splice(index, 1);
      this.list.splice(index, 1);
      this.index--;
      for (let i = index; i < this.list.length; i++) {
        this.list[i].order_id--;
      }
    }
  }
  correctAns(event: any, index: number) {
    console.log(event.target.value, index);
    if (this.options[index]) {
      this.options[index].is_correct = !this.options[index].is_correct;
    }
    this.list[index].is_correct = !this.list[index].is_correct;
    console.log(this.list);
  }
  save() {
    if (this.input.length > 0) {
      this.onEnter();
    }
    let i = 0;
    console.log(this.inputTxt);
    for (i = 0; i < this.list.length; i++) {
      this.list[i].short_input = this.inputTxt[i];
      this.list[i].long_input = this.inputTxt[i];
      if (this.options.length > 0 && i < this.options.length) {
        this.httpService
          .updateChoice(this.options[i].uuid, this.list[i])
          .subscribe();
      } else {
        this.httpService.postChoice(this.item_uuid, this.list[i]).subscribe();
      }
    }

    if (i === this.list.length) {
      this.httpService.postItemID(this.item_uuid, this.item).subscribe(() => {
        console.log(this.item);
        this.routerLink.navigate(['/add-items'], {
          queryParams: {
            is_inside_course: this.inside_course,
            course_uuid: this.crs_uuid,
            screenId: this.screenID,
            uuid: this.coll_uuid,
            examId: this.exam_uuid,
            secId: this.section_uuid,
            resId: this.resourceID,
          },
        });
      });
    }
  }
  saveAdd() {
    if (this.input.length > 0) {
      this.onEnter();
    }
    let i = 0;
    for (i = 0; i < this.list.length; i++) {
      this.httpService.postChoice(this.item_uuid, this.list[i]).subscribe();
    }
    if (i === this.list.length) {
      this.httpService.postItemID(this.item_uuid, this.item).subscribe(() => {
        this.routerLink.navigate(['/item-types'], {
          queryParams: {
            is_inside_course: this.inside_course,
            course_uuid: this.crs_uuid,
            screenId: this.screenID,
            uuid: this.coll_uuid,
            examId: this.exam_uuid,
            secId: this.section_uuid,
            resId: this.resourceID,
          },
        });
      });
    }
  }
  setPicIndex(index: number) {
    if (index === this.list.length) {
      this.newImg = true;
    }
    this.picIndex = index;
    this.clicked = true;
    console.log(this.picIndex);
  }
  delPic() {
    this.item.media_url = '';
  }
  delPicOpt(index: number) {
    this.list[index].image_url = '';
  }
}
