import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { AddItemsComponent } from '../add-items/add-items.component';

@Component({
  selector: 'app-delete-item-dialog',
  templateUrl: './delete-item-dialog.component.html',
  styleUrls: ['./delete-item-dialog.component.css'],
})
export class DeleteItemDialogComponent implements OnInit {
  constructor(public dialogRef: MatDialogRef<AddItemsComponent>) {}

  ngOnInit(): void {}

  onCancel() {
    this.dialogRef.close();
  }
  onDelete() {
    this.dialogRef.close(1);
  }
}
