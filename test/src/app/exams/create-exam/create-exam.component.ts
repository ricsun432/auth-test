import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { mergeMap } from 'rxjs/operators';
import { HttpService } from 'src/app/http.service';
import { SecBlock } from 'src/app/section-block';
import { Exam } from 'src/exam';
import { Resource } from 'src/resource';

@Component({
  selector: 'app-create-exam',
  templateUrl: './create-exam.component.html',
  styleUrls: ['./create-exam.component.css'],
})
export class CreateExamComponent implements OnInit {
  constructor(
    private httpService: HttpService,
    private routerLink: Router,
    private router: ActivatedRoute
  ) {}

  uploadingImg: boolean = false;
  placeholder: string = './assets/img/Placeholder.png';
  submittedImg: boolean = false;
  requiredImg: string = 'img/jpeg, img/png, img/gif';

  imgUri: string = '';
  exam = new Exam('', '');
  resource = new Resource('', '', 'assessment', '');
  screenID: string = '';
  coll_uuid: string = '';
  exam_uuid: string = '';
  section_uuid: string = '';
  resourceID: string = '';
  inside_course: boolean = false;
  sec_uuid: string = '';
  courseID: string = '';
  block = new SecBlock('', '', {});
  ngOnInit(): void {
    this.router.queryParams.subscribe((params) => {
      this.screenID = params.screenId;
      this.coll_uuid = params.uuid;
      this.inside_course = params.is_inside_course;
      this.resourceID = params.resId;
      this.sec_uuid = params.secId;
      this.courseID = params.course_uuid;
    });
  }

  goAddItems() {
    this.resource.name = this.exam.title;
    this.resource.description = this.exam.description
      ? this.exam.description
      : '';
    if (this.inside_course) {
      this.httpService
        .postExam(this.exam)
        .pipe(
          mergeMap((response) => {
            this.exam_uuid = response.uuid;
            return this.httpService.postTimeSlot(response.uuid);
          }),
          mergeMap((response) => {
            return this.httpService.postExamToResources(
              response.exam,
              response.uuid,
              this.resource,
              this.imgUri
            );
          }),
          mergeMap((response) => {
            delete response.content;
            console.log(response);
            this.block.title = this.resource.name;
            this.block.cover_uri = this.imgUri;
            this.block.content = response;
            return this.httpService.postCourseBlock(this.sec_uuid, this.block);
          }),
          mergeMap(() => {
            return this.httpService.postSection(this.exam_uuid);
          })
        )
        .subscribe((response) => {
          this.section_uuid = response.uuid;
          this.routerLink.navigate(['/add-items'], {
            queryParams: {
              is_inside_course: true,
              course_uuid: this.courseID,
              screenId: this.screenID,
              uuid: this.coll_uuid,
              examId: this.exam_uuid,
              secId: this.section_uuid,
              resId: this.resourceID,
            },
          });
        });
    } else {
      this.httpService
        .postExam(this.exam)
        .pipe(
          mergeMap((response) => {
            this.exam_uuid = response.uuid;
            return this.httpService.postTimeSlot(response.uuid);
          }),
          mergeMap((response) => {
            return this.httpService.postExamToResources(
              response.exam,
              response.uuid,
              this.resource,
              this.imgUri
            );
          }),
          mergeMap((response) => {
            this.resourceID = response.uuid;
            return this.httpService.postCollectionResource(
              response.uuid,
              this.coll_uuid
            );
          }),
          mergeMap(() => {
            return this.httpService.postSection(this.exam_uuid);
          })
        )
        .subscribe((response) => {
          this.section_uuid = response.uuid;
          this.routerLink.navigate(['/add-items'], {
            queryParams: {
              screenId: this.screenID,
              uuid: this.coll_uuid,
              examId: this.exam_uuid,
              secId: this.section_uuid,
              resId: this.resourceID,
            },
          });
        });
    }
  }

  onFileSelected(event: any) {
    const file: File = event.target.files[0];
    if (file) {
      const formData = new FormData();

      formData.append('file', file);

      if (
        file.type == 'image/gif' ||
        file.type == 'image/jpeg' ||
        file.type == 'image/png'
      ) {
        this.uploadingImg = true;
        this.httpService.postFile(formData).subscribe((response) => {
          this.imgUri = response.uri;
          this.submittedImg = true;
          this.uploadingImg = false;
        });
      }
    }
  }
}
