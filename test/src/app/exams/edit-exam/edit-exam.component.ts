import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { mergeMap } from 'rxjs/operators';
import { HttpService } from 'src/app/http.service';
import { Exam } from 'src/exam';
import { Resource } from 'src/resource';

@Component({
  selector: 'app-edit-exam',
  templateUrl: './edit-exam.component.html',
  styleUrls: ['./edit-exam.component.css'],
})
export class EditExamComponent implements OnInit {
  constructor(
    private httpService: HttpService,
    private routerLink: Router,
    private router: ActivatedRoute
  ) {}

  uploadingImg: boolean = false;
  placeholder: string = './assets/img/Placeholder.png';
  submittedImg: boolean = false;
  requiredImg: string = 'img/jpeg, img/png, img/gif';

  imgUri: string = '';
  exam = new Exam('', '');
  resource = new Resource('', '', 'assessment', '');
  screenID: string = '';
  coll_uuid: string = '';
  exam_uuid: string = '';
  exam_timeslot: string = '';
  section_uuid: string = '';
  resourceID: string = '';
  ngOnInit(): void {
    this.router.queryParams
      .pipe(
        mergeMap((params) => {
          this.screenID = params.screenId;
          this.coll_uuid = params.uuid;
          this.resourceID = params.resId;
          this.section_uuid = params.secId;
          return this.httpService.getResourceID(this.resourceID);
        })
      )
      .subscribe((response) => {
        this.resource.name = response.name;
        this.resource.description = response.description;
        this.imgUri = response.cover_uri;
        this.exam_uuid = response.content.exam_details.exam_uuid;
        this.exam_timeslot = response.content.exam_details.exam_timeslot_uuid;
      });
  }

  save() {
    this.httpService
      .updateExamToResources(
        this.exam_uuid,
        this.exam_timeslot,
        this.resource,
        this.imgUri,
        this.resourceID
      )
      .subscribe(() => {
        this.routerLink.navigate(['/cast-editor'], {
          queryParams: {
            screenId: this.screenID,
          },
        });
      });
  }

  delExam() {
    this.httpService.deleteResource(this.resourceID).subscribe(() => {
      this.routerLink.navigate(['/cast-editor'], {
        queryParams: {
          screenId: this.screenID,
        },
      });
    });
  }
  onFileSelected(event: any) {
    const file: File = event.target.files[0];
    if (file) {
      const formData = new FormData();

      formData.append('file', file);

      if (
        file.type == 'image/gif' ||
        file.type == 'image/jpeg' ||
        file.type == 'image/png'
      ) {
        this.uploadingImg = true;
        this.httpService.postFile(formData).subscribe((response) => {
          this.imgUri = response.uri;
          this.submittedImg = true;
          this.uploadingImg = false;
        });
      }
    }
  }
}
