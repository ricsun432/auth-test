import { moveItemInArray } from '@angular/cdk/drag-drop';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { HttpService } from 'src/app/http.service';
import { DeleteItemDialogComponent } from '../delete-item-dialog/delete-item-dialog.component';

@Component({
  selector: 'app-add-items',
  templateUrl: './add-items.component.html',
  styleUrls: ['./add-items.component.css'],
})
export class AddItemsComponent implements OnInit {
  constructor(
    private route: ActivatedRoute,
    private httpService: HttpService,
    private routerLink: Router,
    private dialog: MatDialog
  ) {}
  coll_uuid: string = '';
  screenID: string = '';
  exam_uuid: string = '';
  section_uuid: string = '';
  resourceID: string = '';
  item_uuid: string = '';
  items: any = [];
  types: any = ['MC', 'LA', 'PO', 'CB'];
  routes: any = ['/multi-choice', '/text', '/poll', '/checkbox'];
  inside_course: boolean = false;
  crs_uuid: string = '';
  ngOnInit(): void {
    this.route.queryParams
      .pipe(
        mergeMap((params) => {
          this.screenID = params.screenId;
          this.coll_uuid = params.uuid;
          this.exam_uuid = params.examId;
          this.section_uuid = params.secId;
          this.resourceID = params.resId;
          this.inside_course = params.is_inside_course;
          this.crs_uuid = params.course_uuid;
          return this.httpService.getResourceID(this.resourceID);
        }),
        mergeMap((response) => {
          return this.httpService.getSectionList(this.exam_uuid);
        }),
        mergeMap((response) => {
          return this.httpService.getItems(response[0].uuid);
        })
      )
      .subscribe((items) => {
        this.items = items;
      });
  }

  addQuestion() {
    if (this.inside_course) {
      this.routerLink.navigate(['/item-types'], {
        queryParams: {
          is_inside_course: true,
          course_uuid: this.crs_uuid,
          screenId: this.screenID,
          uuid: this.coll_uuid,
          examId: this.exam_uuid,
          secId: this.section_uuid,
          resId: this.resourceID,
        },
      });
    } else {
      this.routerLink.navigate(['/item-types'], {
        queryParams: {
          screenId: this.screenID,
          uuid: this.coll_uuid,
          examId: this.exam_uuid,
          secId: this.section_uuid,
          resId: this.resourceID,
        },
      });
    }
  }
  done() {
    if (this.inside_course) {
      this.routerLink.navigate(['/course-content'], {
        queryParams: {
          screenId: this.screenID,
          course_uuid: this.crs_uuid,
          resId: this.resourceID,
          uuid: this.coll_uuid,
        },
      });
    } else {
      this.routerLink.navigate(['/cast-editor'], {
        queryParams: {
          screenId: this.screenID,
        },
      });
    }
  }

  edit(index: number) {
    console.log(index);
    for (let i = 0; i < this.types.length; i++) {
      if (this.items[index].type === this.types[i]) {
        this.routerLink.navigate([this.routes[i]], {
          queryParams: {
            screenId: this.screenID,
            uuid: this.coll_uuid,
            examId: this.exam_uuid,
            secId: this.section_uuid,
            resId: this.resourceID,
            itemId: this.items[index].uuid,
          },
        });
      }
    }
  }

  delete(index: number) {
    const dialogRef = this.dialog.open(DeleteItemDialogComponent, {
      width: '400px',
      height: '130px',
    });
    dialogRef
      .afterClosed()
      .pipe(
        mergeMap((confirm) => {
          if (confirm) {
            console.log('The dialog was closed');

            return this.httpService.deleteItem(this.items[index].uuid);
          }
          return EMPTY;
        })
      )
      .subscribe(() => {
        this.items.splice(index, 1);
        console.log(this.items);
      });
  }
  drop(event: any) {
    moveItemInArray(this.items, event.previousIndex, event.currentIndex);

    for (let i = 0; i < this.items.length; i++) {
      this.items[i].order_id = i;
      this.httpService
        .postItemID(this.items[i].uuid, this.items[i], i)
        .subscribe();
    }
  }
}
