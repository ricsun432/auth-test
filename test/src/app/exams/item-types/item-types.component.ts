import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpService } from 'src/app/http.service';

@Component({
  selector: 'app-item-types',
  templateUrl: './item-types.component.html',
  styleUrls: ['./item-types.component.css'],
})
export class ItemTypesComponent implements OnInit {
  constructor(
    private routerLink: Router,
    private httpService: HttpService,
    private route: ActivatedRoute
  ) {}
  section_uuid: string = '';
  screenID: string = '';
  coll_uuid: string = '';
  item_uuid: string = '';
  exam_uuid: string = '';
  resourceID: string = '';
  types = ['MC', 'LA', 'CB', 'PO'];
  nav = ['/multi-choice', '/text', '/checkbox', '/poll'];
  inside_course: boolean = false;
  crs_uuid: string = '';
  ngOnInit(): void {
    this.route.queryParams.subscribe((params) => {
      this.inside_course = params.is_inside_course;
      this.crs_uuid = params.course_uuid;
      this.section_uuid = params.secId;
      this.screenID = params.screenId;
      this.coll_uuid = params.uuid;
      this.resourceID = params.resId;
      this.exam_uuid = params.examId;
    });
  }

  postType(index: number) {
    if (this.inside_course) {
      this.httpService
        .postItem(this.section_uuid, this.types[index])
        .subscribe((response) => {
          this.item_uuid = response.uuid;
          this.routerLink.navigate([this.nav[index]], {
            queryParams: {
              is_inside_course: true,
              course_uuid: this.crs_uuid,
              screenId: this.screenID,
              uuid: this.coll_uuid,
              examId: this.exam_uuid,
              secId: this.section_uuid,
              resId: this.resourceID,
              itemId: this.item_uuid,
            },
          });
        });
    } else {
      this.httpService
        .postItem(this.section_uuid, this.types[index])
        .subscribe((response) => {
          this.item_uuid = response.uuid;
          this.routerLink.navigate([this.nav[index]], {
            queryParams: {
              screenId: this.screenID,
              uuid: this.coll_uuid,
              examId: this.exam_uuid,
              secId: this.section_uuid,
              resId: this.resourceID,
              itemId: this.item_uuid,
            },
          });
        });
    }
  }
}
