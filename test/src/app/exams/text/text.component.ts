import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { mergeMap } from 'rxjs/operators';
import { HttpService } from 'src/app/http.service';
import { Item } from 'src/app/item';

@Component({
  selector: 'app-text',
  templateUrl: './text.component.html',
  styleUrls: ['./text.component.css'],
})
export class TextComponent implements OnInit {
  uploadingImg: boolean = false;
  imgUri: string = '';
  submittedImg: boolean = false;
  requiredImg: string = 'image/png, image/jpeg, image/gif';
  item = new Item(
    '',
    'Question',
    'LA',
    true,
    '1.00',
    '',
    '',
    '',
    '',
    false,
    false
  );
  screenID: string = '';
  coll_uuid: string = '';
  exam_uuid: string = '';
  section_uuid: string = '';
  resourceID: string = '';
  item_uuid: string = '';
  inside_course: boolean = false;
  crs_uuid: string = '';

  constructor(
    private httpService: HttpService,
    private route: ActivatedRoute,
    private routerLink: Router
  ) {}

  ngOnInit(): void {
    this.route.queryParams
      .pipe(
        mergeMap((params) => {
          this.inside_course = params.is_inside_course;
          this.crs_uuid = params.course_uuid;
          this.screenID = params.screenId;
          this.coll_uuid = params.uuid;
          this.exam_uuid = params.examId;
          this.section_uuid = params.secId;
          this.resourceID = params.resId;
          this.item_uuid = params.itemId;
          return this.httpService.getItem(this.item_uuid);
        })
      )
      .subscribe((response) => {
        this.item.text = response.text;
        this.item.media_url = response.media_url;
        this.item.required = response.required;
      });
  }
  save() {
    this.httpService.postItemID(this.item_uuid, this.item).subscribe(() => {
      this.routerLink.navigate(['/add-items'], {
        queryParams: {
          is_inside_course: this.inside_course,
          course_uuid: this.crs_uuid,
          screenId: this.screenID,
          uuid: this.coll_uuid,
          examId: this.exam_uuid,
          secId: this.section_uuid,
          resId: this.resourceID,
        },
      });
    });
  }
  saveAdd() {
    this.httpService.postItemID(this.item_uuid, this.item).subscribe(() => {
      this.routerLink.navigate(['/item-types'], {
        queryParams: {
          is_inside_course: this.inside_course,
          course_uuid: this.crs_uuid,
          screenId: this.screenID,
          uuid: this.coll_uuid,
          examId: this.exam_uuid,
          secId: this.section_uuid,
          resId: this.resourceID,
        },
      });
    });
  }
  onChange() {
    console.log(this.item.required);
  }

  onFileSelected(event: any) {
    const file: File = event.target.files[0];
    if (file) {
      const formData = new FormData();

      formData.append('file', file);
      if (
        file.type == 'image/gif' ||
        file.type == 'image/jpeg' ||
        file.type == 'image/png'
      ) {
        this.uploadingImg = true;
        this.httpService.postFile(formData).subscribe((response) => {
          this.imgUri = response.uri;
          this.item.media_url = response.uri;
          this.submittedImg = true;
          this.uploadingImg = false;
        });
      }
    }
  }
}
