import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AddItemsComponent } from './add-items/add-items.component';
import { CheckboxComponent } from './checkbox/checkbox.component';
import { CreateExamComponent } from './create-exam/create-exam.component';
import { DeleteItemDialogComponent } from './delete-item-dialog/delete-item-dialog.component';
import { EditExamComponent } from './edit-exam/edit-exam.component';
import { ExamProfileComponent } from './exam-profile/exam-profile.component';
import { ExamsRoutingModule } from './exams.routing';
import { ItemTypesComponent } from './item-types/item-types.component';
import { MultiChoiceComponent } from './multi-choice/multi-choice.component';
import { PollComponent } from './poll/poll.component';
import { TextComponent } from './text/text.component';

@NgModule({
  declarations: [
    AddItemsComponent,
    CheckboxComponent,
    CreateExamComponent,
    EditExamComponent,
    ItemTypesComponent,
    MultiChoiceComponent,
    PollComponent,
    TextComponent,
    DeleteItemDialogComponent,
    ExamProfileComponent,
  ],
  imports: [
    CommonModule,
    ExamsRoutingModule,
    DragDropModule,
    FormsModule,
    HttpClientModule,
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
  ],
})
export class ExamsModule {}
