import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { mergeMap } from 'rxjs/operators';
import { HttpService } from 'src/app/http.service';

@Component({
  selector: 'app-exam-profile',
  templateUrl: './exam-profile.component.html',
  styleUrls: ['./exam-profile.component.css'],
})
export class ExamProfileComponent implements OnInit {
  constructor(
    private routerLink: Router,
    private route: ActivatedRoute,
    private httpService: HttpService
  ) {}

  resourceID: string = '';
  screenID: string = '';
  exam_uuid: string = '';
  sec_uuid: string = '';
  coll_uuid: string = '';
  ngOnInit(): void {
    this.route.queryParams
      .pipe(
        mergeMap((params) => {
          this.resourceID = params.resId;
          this.screenID = params.screenId;
          this.coll_uuid = params.uuid;
          return this.httpService.getResourceID(this.resourceID);
        }),
        mergeMap((response) => {
          console.log(response);
          this.exam_uuid = response.content.exam_details.exam_uuid;
          return this.httpService.getSectionList(this.exam_uuid);
        })
      )
      .subscribe((response) => {
        this.sec_uuid = response[0].uuid;
      });
  }

  editResource() {
    this.routerLink.navigate(['/edit-exam'], {
      queryParams: {
        screenId: this.screenID,
        uuid: this.coll_uuid,
        resId: this.resourceID,
        secId: this.sec_uuid,
      },
    });
  }

  editExam() {
    this.routerLink.navigate(['/add-items'], {
      queryParams: {
        screenId: this.screenID,
        uuid: this.coll_uuid,
        examId: this.exam_uuid,
        secId: this.sec_uuid,
        resId: this.resourceID,
      },
    });
  }
}
