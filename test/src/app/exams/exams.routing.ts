import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddItemsComponent } from './add-items/add-items.component';
import { CheckboxComponent } from './checkbox/checkbox.component';
import { CreateExamComponent } from './create-exam/create-exam.component';
import { EditExamComponent } from './edit-exam/edit-exam.component';
import { ExamProfileComponent } from './exam-profile/exam-profile.component';
import { ItemTypesComponent } from './item-types/item-types.component';
import { MultiChoiceComponent } from './multi-choice/multi-choice.component';
import { PollComponent } from './poll/poll.component';
import { TextComponent } from './text/text.component';

const routes: Routes = [
  { path: 'create-exam', component: CreateExamComponent },
  { path: 'add-items', component: AddItemsComponent },
  { path: 'checkbox', component: CheckboxComponent },
  { path: 'edit-exam', component: EditExamComponent },
  { path: 'item-types', component: ItemTypesComponent },
  { path: 'multi-choice', component: MultiChoiceComponent },
  { path: 'poll', component: PollComponent },
  { path: 'text', component: TextComponent },
  { path: 'exam-profile', component: ExamProfileComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ExamsRoutingModule {}
