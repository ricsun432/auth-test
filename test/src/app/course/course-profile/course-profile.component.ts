import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { mergeMap } from 'rxjs/operators';
import { HttpService } from 'src/app/http.service';

@Component({
  selector: 'app-course-profile',
  templateUrl: './course-profile.component.html',
  styleUrls: ['./course-profile.component.css'],
})
export class CourseProfileComponent implements OnInit {
  constructor(
    private routerLink: Router,
    private route: ActivatedRoute,
    private httpService: HttpService
  ) {}

  coll_uuid: string = '';
  resourceID: string = '';
  screenID: string = '';
  crs_uuid: string = '';
  ngOnInit(): void {
    this.route.queryParams
      .pipe(
        mergeMap((params) => {
          this.coll_uuid = params.uuid;
          this.resourceID = params.resId;
          this.screenID = params.screenId;
          return this.httpService.getResourceID(this.resourceID);
        })
      )
      .subscribe((response) => {
        this.crs_uuid = response.content.course_details.course_uuid;
      });
  }
  editCourse() {
    this.routerLink.navigate(['/course-content'], {
      queryParams: {
        screenId: this.screenID,
        course_uuid: this.crs_uuid,
        resId: this.resourceID,
        uuid: this.coll_uuid,
      },
    });
  }
  editResource() {
    this.routerLink.navigate(['/edit-course'], {
      queryParams: {
        screenId: this.screenID,
        course_uuid: this.crs_uuid,
        resId: this.resourceID,
        uuid: this.coll_uuid,
      },
    });
  }
}
