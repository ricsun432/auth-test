import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CourseContentComponent } from './course-content/course-content.component';
import { CourseProfileComponent } from './course-profile/course-profile.component';
import { CreateCourseComponent } from './create-course/create-course.component';
import { EditCourseComponent } from './edit-course/edit-course.component';

const routes: Routes = [
  { path: 'create-course', component: CreateCourseComponent },
  { path: 'edit-course', component: EditCourseComponent },
  { path: 'course-profile', component: CourseProfileComponent },
  { path: 'course-content', component: CourseContentComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CourseRoutingModule {}
