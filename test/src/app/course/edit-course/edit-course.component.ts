import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { mergeMap } from 'rxjs/operators';
import { Course } from 'src/app/course';
import { HttpService } from 'src/app/http.service';
import { Resource } from 'src/resource';

@Component({
  selector: 'app-edit-course',
  templateUrl: './edit-course.component.html',
  styleUrls: ['./edit-course.component.css'],
})
export class EditCourseComponent implements OnInit {
  constructor(
    private httpService: HttpService,
    private route: ActivatedRoute,
    private routerLink: Router
  ) {}
  uploadingImg: boolean = false;
  placeholder: string = './assets/img/Placeholder.png';
  submittedImg: boolean = false;
  requiredImg: string = 'img/jpeg, img/png, img/gif';
  imgUri: string = '';
  course_res = new Resource('', '', 'course', '');
  course = new Course('', '');
  screenID: string = '';
  coll_uuid: string = '';
  crs_uuid: string = '';
  exam_uuid: string = '';
  section_uuid: string = '';
  resourceID: string = '';
  inside_course: boolean = false;
  description: string = '';
  ngOnInit(): void {
    this.route.queryParams
      .pipe(
        mergeMap((params) => {
          this.coll_uuid = params.uuid;
          this.screenID = params.screenId;
          this.resourceID = params.resId;
          this.crs_uuid = params.course_uuid;
          this.inside_course = params.is_inside_course;
          return this.httpService.getResourceID(this.resourceID);
        })
      )
      .subscribe((response) => {
        console.log(response);
        this.imgUri = response.cover_uri;
        this.course.title = response.name;
        this.course_res.description = response.description;
      });
  }
  goBack() {
    this.course_res.name = this.course.title;
    this.course.cover_uri = this.imgUri;
    this.description = this.course_res.description;

    this.httpService
      .updateCourse(this.crs_uuid, this.course)
      .pipe(
        mergeMap(() => {
          this.course_res.description = this.description;
          return this.httpService.updateCourseInResources(
            this.crs_uuid,
            this.resourceID,
            this.course_res,
            this.imgUri,
            this.coll_uuid
          );
        })
      )
      .subscribe(() => {
        if (this.inside_course) {
          this.routerLink.navigate(['/course-content'], {
            queryParams: {
              screenId: this.screenID,
              course_uuid: this.crs_uuid,
              resId: this.resourceID,
              uuid: this.coll_uuid,
            },
          });
        } else {
          this.routerLink.navigate(['/cast-editor'], {
            queryParams: { screenId: this.screenID },
          });
        }
      });
  }
  onFileSelected(event: any) {
    const file: File = event.target.files[0];
    if (file) {
      const formData = new FormData();

      formData.append('file', file);

      if (
        file.type == 'image/gif' ||
        file.type == 'image/jpeg' ||
        file.type == 'image/png'
      ) {
        this.uploadingImg = true;
        this.httpService.postFile(formData).subscribe((response) => {
          this.imgUri = response.uri;
          this.submittedImg = true;
          this.uploadingImg = false;
        });
      }
    }
  }
  goCourseContent() {
    this.routerLink.navigate(['/course-content'], {
      queryParams: {
        screenId: this.screenID,
        course_uuid: this.crs_uuid,
        resId: this.resourceID,
        uuid: this.coll_uuid,
      },
    });
  }
}
