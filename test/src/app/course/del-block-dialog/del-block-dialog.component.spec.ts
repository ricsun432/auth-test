import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DelBlockDialogComponent } from './del-block-dialog.component';

describe('DelBlockDialogComponent', () => {
  let component: DelBlockDialogComponent;
  let fixture: ComponentFixture<DelBlockDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DelBlockDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DelBlockDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
