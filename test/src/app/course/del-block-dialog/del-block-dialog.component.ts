import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { CourseContentComponent } from '../course-content/course-content.component';

@Component({
  selector: 'app-del-block-dialog',
  templateUrl: './del-block-dialog.component.html',
  styleUrls: ['./del-block-dialog.component.css'],
})
export class DelBlockDialogComponent implements OnInit {
  constructor(public dialogRef: MatDialogRef<CourseContentComponent>) {}

  ngOnInit(): void {}
  onCancel() {
    this.dialogRef.close();
  }

  onDelete() {
    this.dialogRef.close(1);
  }
}
