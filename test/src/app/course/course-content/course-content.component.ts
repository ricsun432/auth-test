import { moveItemInArray } from '@angular/cdk/drag-drop';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { HttpService } from 'src/app/http.service';
import { Section } from 'src/app/section';
import { AddSectionDialogComponent } from '../add-section-dialog/add-section-dialog.component';
import { DelBlockDialogComponent } from '../del-block-dialog/del-block-dialog.component';
import { DelSectionDialogComponent } from '../del-section-dialog/del-section-dialog.component';

@Component({
  selector: 'app-course-content',
  templateUrl: './course-content.component.html',
  styleUrls: ['./course-content.component.css'],
})
export class CourseContentComponent implements OnInit {
  constructor(
    private httpService: HttpService,
    private route: ActivatedRoute,
    private routerLink: Router,
    private dialog: MatDialog
  ) {}

  crs_uuid: string = '';
  coll_uuid: string = '';
  screenID: string = '';
  resourceID: string = '';
  crs_title: string = '';
  editable: Array<boolean> = [];
  sections: any = [];
  sec = new Section('', '00');
  ngOnInit(): void {
    this.route.queryParams
      .pipe(
        mergeMap((params) => {
          this.crs_uuid = params.course_uuid;
          this.coll_uuid = params.uuid;
          this.resourceID = params.resId;
          this.screenID = params.screenId;
          return this.httpService.getCourse(this.crs_uuid);
        }),
        mergeMap((response) => {
          this.crs_title = response.title;
          return this.httpService.getCourseSections(this.crs_uuid);
        })
      )
      .subscribe((response) => {
        this.sections = response.outline;
        console.log(this.sections);
        this.editable = [];
        for (let i = 0; i < response.outline.length; i++) {
          this.editable.push(false);
        }
      });
  }
  next() {
    this.routerLink.navigate(['/cast-editor'], {
      queryParams: {
        screenId: this.screenID,
      },
    });
  }
  addSection(index?: number) {
    const dialogRef = this.dialog.open(AddSectionDialogComponent, {
      width: '400px',
      height: '200px',
    });
    dialogRef
      .afterClosed()
      .pipe(
        mergeMap((input) => {
          if (input) {
            console.log('The dialog was closed');
            this.sec.title = input;
            if (index) {
              if (index > 9) {
                this.sec.order_id = index.toString();
              } else {
                this.sec.order_id = '0' + index.toString();
              }
            }
            console.log(this.sec.order_id);
            return this.httpService.postCourseSection(this.crs_uuid, this.sec);
          }
          return EMPTY;
        })
      )
      .subscribe(() => {
        this.ngOnInit();
      });
  }
  editCourseResource() {
    this.routerLink.navigate(['/edit-course'], {
      queryParams: {
        is_inside_course: true,
        course_uuid: this.crs_uuid,
        resId: this.resourceID,
        uuid: this.coll_uuid,
        screenId: this.screenID,
      },
    });
  }

  edit(index: number) {
    this.editable[index] = true;
  }
  editSection(index: number) {
    this.sec.title = this.sections[index].title;
    this.httpService
      .updateCourseSection(this.sections[index].uuid, this.sec)
      .subscribe(() => {
        this.editable[index] = false;
      });
  }
  deleteSection(index: number) {
    const dialogRef = this.dialog.open(DelSectionDialogComponent, {
      width: '400px',
      height: '130px',
    });
    dialogRef
      .afterClosed()
      .pipe(
        mergeMap((confirm) => {
          if (confirm) {
            console.log('The dialog was closed');

            return this.httpService.deleteCourseSection(
              this.sections[index].uuid
            );
          }
          return EMPTY;
        })
      )
      .subscribe(() => {
        this.ngOnInit();
      });
  }
  deleteBlock(sec_index: number, block_index: number) {
    const dialogRef = this.dialog.open(DelBlockDialogComponent, {
      width: '400px',
      height: '130px',
    });
    dialogRef
      .afterClosed()
      .pipe(
        mergeMap((confirm) => {
          if (confirm) {
            console.log('The dialog was closed');
            return this.httpService.deleteCourseSectionBlock(
              this.sections[sec_index].blocks[block_index].uuid
            );
          }
          return EMPTY;
        })
      )
      .subscribe(() => {
        this.ngOnInit();
      });
  }
  addBlock(sec_index: number, block_index?: number) {
    this.routerLink.navigate(['/resource-options'], {
      queryParams: {
        is_inside_course: true,
        course_uuid: this.crs_uuid,
        secId: this.sections[sec_index].uuid,
        resId: this.resourceID,
        uuid: this.coll_uuid,
        screenId: this.screenID,
      },
    });
  }
  dropSection(event: any) {
    moveItemInArray(this.sections, event.previousIndex, event.currentIndex);
    for (let i = 0; i < this.sections.length; i++) {
      this.sections[i].order_id = i > 9 ? i.toString() : '0' + i.toString();
      this.httpService
        .updateCourseSectionOrder(
          this.sections[i].uuid,
          this.sections[i].order_id
        )
        .subscribe();
    }
  }
  dropBlock(event: any, index: number) {
    moveItemInArray(
      this.sections[index].blocks,
      event.previousIndex,
      event.currentIndex
    );

    for (let i = 0; i < this.sections[index].blocks.length; i++) {
      this.sections[index].blocks[i].order_id =
        i > 9 ? i.toString() : '0' + i.toString();
      this.httpService
        .updateCourseBlockOrder(
          this.sections[index].uuid,
          this.sections[index].blocks[i].uuid,
          this.sections[index].blocks[i].order_id
        )
        .subscribe();
    }
  }
}
