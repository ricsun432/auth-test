import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { CourseContentComponent } from '../course-content/course-content.component';

@Component({
  selector: 'app-add-section-dialog',
  templateUrl: './add-section-dialog.component.html',
  styleUrls: ['./add-section-dialog.component.css'],
})
export class AddSectionDialogComponent implements OnInit {
  constructor(public dialogRef: MatDialogRef<CourseContentComponent>) {}

  input: string = '';
  ngOnInit(): void {}
  onCancel() {
    this.dialogRef.close();
  }
  onSubmit() {
    this.dialogRef.close(this.input);
  }
}
