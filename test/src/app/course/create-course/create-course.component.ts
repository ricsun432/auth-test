import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { mergeMap } from 'rxjs/operators';
import { Course } from 'src/app/course';
import { HttpService } from 'src/app/http.service';
import { Resource } from 'src/resource';

@Component({
  selector: 'app-create-course',
  templateUrl: './create-course.component.html',
  styleUrls: ['./create-course.component.css'],
})
export class CreateCourseComponent implements OnInit {
  constructor(
    private httpService: HttpService,
    private router: ActivatedRoute,
    private routerLink: Router
  ) {}

  uploadingImg: boolean = false;
  placeholder: string = './assets/img/Placeholder.png';
  submittedImg: boolean = false;
  requiredImg: string = 'img/jpeg, img/png, img/gif';

  imgUri: string = '';
  course_res = new Resource('', '', 'course', '');
  course = new Course('', '');
  screenID: string = '';
  coll_uuid: string = '';
  crs_uuid: string = '';
  exam_uuid: string = '';
  section_uuid: string = '';
  resourceID: string = '';
  description: string = '';
  ngOnInit(): void {
    this.router.queryParams.subscribe((params) => {
      this.screenID = params.screenId;
      this.resourceID = params.resId;
      this.coll_uuid = params.uuid;
    });
  }

  goAddItems() {
    this.course_res.name = this.course.title;
    this.course.cover_uri = this.imgUri;
    this.description = this.course_res.description;
    this.httpService
      .postCourse(this.course)
      .pipe(
        mergeMap((response) => {
          this.course_res.description = this.description;
          console.log(this.course_res.description);
          this.crs_uuid = response.uuid;
          return this.httpService.postCourseToResources(
            response.uuid,
            this.course_res,
            this.imgUri,
            this.coll_uuid
          );
        })
      )
      .subscribe((response) => {
        this.resourceID = response.uuid;
        this.routerLink.navigate(['/course-content'], {
          queryParams: {
            screenId: this.screenID,
            course_uuid: this.crs_uuid,
            resId: this.resourceID,
            uuid: this.coll_uuid,
          },
        });
      });
  }

  onFileSelected(event: any) {
    const file: File = event.target.files[0];
    if (file) {
      const formData = new FormData();

      formData.append('file', file);

      if (
        file.type == 'image/gif' ||
        file.type == 'image/jpeg' ||
        file.type == 'image/png'
      ) {
        this.uploadingImg = true;
        this.httpService.postFile(formData).subscribe((response) => {
          this.imgUri = response.uri;
          this.submittedImg = true;
          this.uploadingImg = false;
        });
      }
    }
  }
}
