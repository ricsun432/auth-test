import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { CourseContentComponent } from '../course-content/course-content.component';

@Component({
  selector: 'app-del-section-dialog',
  templateUrl: './del-section-dialog.component.html',
  styleUrls: ['./del-section-dialog.component.css'],
})
export class DelSectionDialogComponent implements OnInit {
  constructor(public dialogRef: MatDialogRef<CourseContentComponent>) {}

  ngOnInit(): void {}
  onCancel() {
    this.dialogRef.close();
  }

  onDelete() {
    this.dialogRef.close(1);
  }
}
