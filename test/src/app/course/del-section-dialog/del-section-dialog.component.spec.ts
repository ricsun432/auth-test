import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DelSectionDialogComponent } from './del-section-dialog.component';

describe('DelSectionDialogComponent', () => {
  let component: DelSectionDialogComponent;
  let fixture: ComponentFixture<DelSectionDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DelSectionDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DelSectionDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
