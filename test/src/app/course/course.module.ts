import { DragDropModule } from '@angular/cdk/drag-drop';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatCommonModule } from '@angular/material/core';
import { MatDialogModule } from '@angular/material/dialog';
import { AddSectionDialogComponent } from './add-section-dialog/add-section-dialog.component';
import { CourseContentComponent } from './course-content/course-content.component';
import { CourseProfileComponent } from './course-profile/course-profile.component';
import { CourseRoutingModule } from './course.routing';
import { CreateCourseComponent } from './create-course/create-course.component';
import { DelBlockDialogComponent } from './del-block-dialog/del-block-dialog.component';
import { DelSectionDialogComponent } from './del-section-dialog/del-section-dialog.component';
import { EditCourseComponent } from './edit-course/edit-course.component';

@NgModule({
  declarations: [
    EditCourseComponent,
    CreateCourseComponent,
    CourseProfileComponent,
    CourseContentComponent,
    AddSectionDialogComponent,
    DelBlockDialogComponent,
    DelSectionDialogComponent,
  ],
  imports: [
    CommonModule,
    CourseRoutingModule,
    FormsModule,
    DragDropModule,
    HttpClientModule,
    MatCommonModule,
    MatDialogModule,
    ScrollingModule,
    ReactiveFormsModule,
  ],
})
export class CourseModule {}
