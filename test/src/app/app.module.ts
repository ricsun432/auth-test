import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CastModule } from './cast/cast.module';
import { CourseModule } from './course/course.module';
import { ExamsModule } from './exams/exams.module';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { ProfileModule } from './profile/profile.module';
import { ResourceModule } from './resource/resource.module';

@NgModule({
  declarations: [AppComponent, PageNotFoundComponent, NavBarComponent],
  imports: [
    CommonModule,
    BrowserModule,
    ExamsModule,
    CastModule,
    ResourceModule,
    ProfileModule,
    CourseModule,
    AppRoutingModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
