import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { ScreensComponent } from '../screens/screens.component';

@Component({
  selector: 'app-create-screen',
  templateUrl: './create-screen.component.html',
  styleUrls: ['./create-screen.component.css'],
})
export class CreateScreenComponent implements OnInit {
  constructor(public dialogRef: MatDialogRef<ScreensComponent>) {}

  ngOnInit(): void {}

  name: string = '';
  onCancel() {
    this.dialogRef.close();
  }

  onEnter() {
    this.dialogRef.close(this.name);
  }
}
