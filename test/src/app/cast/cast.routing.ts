import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BlockDialogComponent } from './block-dialog/block-dialog.component';
import { CarouselBlockComponent } from './carousel-block/carousel-block.component';
import { CastEditorComponent } from './cast-editor/cast-editor.component';
import { CatalogBlockComponent } from './catalog-block/catalog-block.component';
import { CreateScreenComponent } from './create-screen/create-screen.component';
import { DeleteBlockDialogComponent } from './delete-block-dialog/delete-block-dialog.component';
import { DeleteDialogComponent } from './delete-dialog/delete-dialog.component';
import { DeleteResourceDialogComponent } from './delete-resource-dialog/delete-resource-dialog.component';
import { EditScreenComponent } from './edit-screen/edit-screen.component';
import { ScreensComponent } from './screens/screens.component';
import { TextBlockComponent } from './text-block/text-block.component';

const routes: Routes = [
  { path: 'cast-editor', component: CastEditorComponent },
  { path: 'catalog-block', component: CatalogBlockComponent },
  { path: 'create-screen', component: CreateScreenComponent },
  { path: 'delete-block-dialog', component: DeleteBlockDialogComponent },
  { path: 'delete-dialog', component: DeleteDialogComponent },
  { path: 'delete-resource-dialog', component: DeleteResourceDialogComponent },
  { path: 'edit-screen', component: EditScreenComponent },
  { path: 'text-block', component: TextBlockComponent },
  { path: 'screens', component: ScreensComponent },
  { path: 'block-dialog', component: BlockDialogComponent },
  { path: 'carousel-block', component: CarouselBlockComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CastRoutingModule {}
