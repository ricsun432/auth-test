import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { DeleteDialogComponent } from 'src/app/cast/delete-dialog/delete-dialog.component';
import { HttpService } from 'src/app/http.service';
import { Screen } from 'src/app/screen';
import { environment } from 'src/environments/environment';
@Component({
  selector: 'app-edit-screen',
  templateUrl: './edit-screen.component.html',
  styleUrls: ['./edit-screen.component.css'],
})
export class EditScreenComponent implements OnInit {
  constructor(
    private routerLink: Router,
    private httpService: HttpService,
    private router: ActivatedRoute,
    public dialog: MatDialog
  ) {}
  list: any = [];
  screen = new Screen('', '', '', 42);
  screenId = '';
  prevId = '';
  ngOnInit(): void {
    this.router.queryParams
      .pipe(
        mergeMap((params) => {
          this.screenId = params.screenId;
          this.prevId = params.screen_id;
          console.log(this.prevId);
          return this.httpService.getScreen(
            environment.cast_id,
            params.screenId
          );
        })
      )
      .subscribe((response) => {
        this.list = response;
        this.screen.title = response.title;
      });
  }

  editScreen() {
    this.routerLink.navigate(['/cast-editor'], {
      queryParams: { screenId: this.screenId },
    });
  }
  save() {
    this.httpService
      .updateScreen(environment.cast_id, this.screenId, this.screen.title)
      .subscribe(() =>
        this.routerLink.navigate(['/screens'], {
          queryParams: { screenId: this.prevId },
        })
      );
  }
  openDialog() {
    const dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '400px',
      height: '130px',
    });
    dialogRef
      .afterClosed()
      .pipe(
        mergeMap((confirm) => {
          if (confirm) {
            console.log('The dialog was closed');

            return this.httpService.deleteScreen(
              environment.cast_id,
              this.screenId
            );
          }
          return EMPTY;
        })
      )
      .subscribe(() => {
        console.log(this.prevId);
        this.routerLink.navigate(['/screens'], {
          queryParams: { screedId: this.prevId },
        });
      });
  }
}
