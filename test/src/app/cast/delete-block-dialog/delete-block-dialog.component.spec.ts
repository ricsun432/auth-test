import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteBlockDialogComponent } from './delete-block-dialog.component';

describe('DeleteBlockDialogComponent', () => {
  let component: DeleteBlockDialogComponent;
  let fixture: ComponentFixture<DeleteBlockDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeleteBlockDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteBlockDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
