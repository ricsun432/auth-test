import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { TextBlockComponent } from '../text-block/text-block.component';

@Component({
  selector: 'app-delete-block-dialog',
  templateUrl: './delete-block-dialog.component.html',
  styleUrls: ['./delete-block-dialog.component.css'],
})
export class DeleteBlockDialogComponent implements OnInit {
  constructor(public dialogRef: MatDialogRef<TextBlockComponent>) {}

  ngOnInit(): void {}
  onCancel() {
    this.dialogRef.close();
  }

  onDelete() {
    this.dialogRef.close(1);
  }
}
