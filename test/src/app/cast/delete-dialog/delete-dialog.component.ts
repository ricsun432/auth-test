import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { EditScreenComponent } from '../edit-screen/edit-screen.component';

@Component({
  selector: 'app-delete-dialog',
  templateUrl: './delete-dialog.component.html',
  styleUrls: ['./delete-dialog.component.css'],
})
export class DeleteDialogComponent implements OnInit {
  constructor(public dialogRef: MatDialogRef<EditScreenComponent>) {}

  ngOnInit(): void {}

  onCancel() {
    this.dialogRef.close();
  }

  onDelete() {
    this.dialogRef.close(1);
  }
}
