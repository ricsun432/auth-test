import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { CatalogBlockComponent } from '../catalog-block/catalog-block.component';

@Component({
  selector: 'app-delete-resource-dialog',
  templateUrl: './delete-resource-dialog.component.html',
  styleUrls: ['./delete-resource-dialog.component.css'],
})
export class DeleteResourceDialogComponent implements OnInit {
  constructor(public dialogRef: MatDialogRef<CatalogBlockComponent>) {}

  ngOnInit(): void {}
  onCancel() {
    this.dialogRef.close();
  }

  onDelete() {
    this.dialogRef.close(1);
  }
}
