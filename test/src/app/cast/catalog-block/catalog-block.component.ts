import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { Block } from 'src/app/block';
import { Collection } from 'src/app/collection';
import { HttpService } from 'src/app/http.service';
import { environment } from 'src/environments/environment';
import { DeleteBlockDialogComponent } from '../delete-block-dialog/delete-block-dialog.component';

@Component({
  selector: 'app-catalog-block',
  templateUrl: './catalog-block.component.html',
  styleUrls: ['./catalog-block.component.css'],
})
export class CatalogBlockComponent implements OnInit {
  editable: boolean = false;
  @Input() content: string = '';
  @Input() screenID: string = '';
  @Input() blockID: number = 1263;
  @Input() block: Block = new Block(0, '', {});
  @Output() deleted = new EventEmitter<boolean>();
  collection = new Collection('New Block');

  constructor(
    private httpService: HttpService,
    private dialog: MatDialog,
    private routerLink: Router
  ) {}

  list: any = [];
  types: any = ['video', 'link', 'pdf', 'assessment', 'course'];
  routes: any = [
    '/edit-video',
    '/edit-link',
    '/edit-pdf',
    '/exam-profile',
    '/course-profile',
  ];
  ngOnInit(): void {
    if (this.content) {
      console.log(this.content);
      this.httpService
        .getCollect(this.content)
        .pipe(
          mergeMap((resp) => {
            this.collection.name = resp.name;
            return this.httpService.getCollectionID(this.content);
          })
        )
        .subscribe((response) => {
          this.list = response;
        });
    }
  }
  addResource() {
    this.routerLink.navigate(['/resource-options'], {
      queryParams: { screenId: this.screenID, uuid: this.content },
    });
    console.log('Resource added');
  }
  edit() {
    this.editable = true;
  }
  editResource(resourceID: string, type: string) {
    console.log('resource edited');

    for (let i = 0; i < this.types.length; i++) {
      console.log(this.routes[i]);
      if (type === this.types[i] && i < 3) {
        this.routerLink.navigate([this.routes[i]], {
          queryParams: { uuid: resourceID, screenId: this.screenID },
        });
        break;
      } else if (type === this.types[i]) {
        this.routerLink.navigate([this.routes[i]], {
          queryParams: {
            uuid: this.content,
            resId: resourceID,
            screenId: this.screenID,
          },
        });
        break;
      }
    }
  }
  save() {
    this.block.properties = { uuid: this.content };
    this.editable = false;
    this.httpService
      .updateCollection(this.content, this.collection)
      .subscribe(console.log);
  }
  delResource(resourceID: string) {
    const dialogRef = this.dialog.open(DeleteBlockDialogComponent, {
      width: '400px',
      height: '130px',
    });
    dialogRef
      .afterClosed()
      .pipe(
        mergeMap((confirm) => {
          if (confirm) {
            console.log('The dialog was closed');
            return this.httpService.deleteResource(resourceID);
          }
          return EMPTY;
        })
      )
      .subscribe(() => {
        this.deleted.emit(true);
      });
  }
  del() {
    const dialogRef = this.dialog.open(DeleteBlockDialogComponent, {
      width: '400px',
      height: '130px',
    });
    dialogRef
      .afterClosed()
      .pipe(
        mergeMap((confirm) => {
          if (confirm) {
            console.log('The dialog was closed');

            return this.httpService.deleteBlock(
              environment.cast_id,
              this.screenID,
              this.blockID
            );
          }
          return EMPTY;
        })
      )
      .subscribe(() => {
        this.deleted.emit(true);
      });
  }
}
