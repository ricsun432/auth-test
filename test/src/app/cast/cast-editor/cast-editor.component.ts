import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { Block } from 'src/app/block';
import { Collection } from 'src/app/collection';
import { HttpService } from 'src/app/http.service';
import { environment } from 'src/environments/environment';
import { BlockDialogComponent } from '../block-dialog/block-dialog.component';

@Component({
  selector: 'app-cast-editor',
  templateUrl: './cast-editor.component.html',
  styleUrls: ['./cast-editor.component.css'],
})
export class CastEditorComponent implements OnInit {
  constructor(
    private httpService: HttpService,
    private routerLink: Router,
    private router: ActivatedRoute,
    public dialog: MatDialog
  ) {
    this.routerLink.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  screenID: string = '';
  homeScreen: string = '';
  query: string = '';
  block = new Block(0, '', {});
  blockID: number = 0;
  content1: string = '';
  blocks: any = [];
  order: number = 0;
  blockIDs: number[] = [];
  uuid: string = '';
  collection = new Collection('New Block');
  ngOnInit(): void {
    this.router.queryParams
      .pipe(
        //returns Cast object
        mergeMap((params) => {
          console.log(params);
          if (Object.keys(params).length === 0) this.query = '';
          else {
            this.query = params.screenId;
          }
          console.log(this.query);
          return this.httpService.getCast(environment.cast_id);
        }),
        //get initial block id block
        mergeMap((cast) => {
          this.homeScreen = cast.properties.home_screen;
          console.log(this.homeScreen);
          return this.httpService.getBlock(
            environment.cast_id,
            '130',
            cast.properties.initial_block_id
          );
        }),
        //get screen list
        mergeMap((block) => {
          console.log(block);
          return this.httpService.getScreenList(environment.cast_id);
        }),
        //get screen according to query params
        //default screen is screen with slug=="home"
        mergeMap((screenList) => {
          console.log(screenList);
          if (this.query === '') {
            for (let screen of screenList) {
              if (screen.slug === this.homeScreen) this.screenID = screen.id;
            }
          } else {
            this.screenID = this.query;
          }

          return this.httpService.getScreen(environment.cast_id, this.screenID);
        }),
        //get list of blocks in the screen selected
        mergeMap(() => {
          return this.httpService.getBlockList(
            environment.cast_id,
            this.screenID
          );
        })
      )
      //assigns blocks array with array of blocks
      .subscribe((blockList) => {
        this.blocks = blockList;
      });
  }
  goScreenList() {
    this.routerLink.navigate(['/screens'], {
      queryParams: { screenId: this.screenID },
    });
  }

  // Handles drag and drop
  drop(event: CdkDragDrop<Block[]>) {
    console.log(this.blocks);
    moveItemInArray(this.blocks, event.previousIndex, event.currentIndex);

    for (let i = 0; i < this.blocks.length; ++i) {
      this.blocks[i].order = i * 10;
      this.httpService
        .updateBlock(
          environment.cast_id,
          this.screenID,
          this.blocks[i].id,
          this.blocks[i]
        )
        .subscribe();
    }

    console.log(this.blocks);
  }

  //opens modal dialog to choose which block to add
  addBlock(index: number) {
    const dialogRef = this.dialog.open(BlockDialogComponent, {
      width: '450px',
      height: '650px',
    });

    dialogRef
      .afterClosed()
      .pipe(
        mergeMap((block) => {
          if (block) {
            if (block === 'carousel') {
              this.block.type = 'carousel';
              this.block.properties = {};
            }
            if (block === 'catalog') {
              this.block.type = 'catalog';
              this.block.properties = {};
            }
            if (block === 'text') {
              this.block.type = 'markdown';
              this.block.properties = {};
            }

            if (index !== this.blocks.length) {
              for (let i = index; i < this.blocks.length; ++i) {
                this.blocks[i].order = (i + 1) * 10;
                this.httpService
                  .updateBlock(
                    environment.cast_id,
                    this.screenID,
                    this.blocks[i].id,
                    this.blocks[i]
                  )
                  .subscribe();
              }
            }
            //add the block
            this.block.order = index * 10;
            return this.httpService.postBlock(
              environment.cast_id,
              this.screenID,
              this.block
            );
          }
          return EMPTY;
        }),
        mergeMap((response) => {
          console.log(`The index for the inserted block is ${index}`);
          console.log(`The length of blocks is ${this.blocks.length}`);
          console.log(`Added ${this.block}`);
          this.blockID = response.id;
          //post Collection then update block to put uuid in properties
          if (this.block.type === 'catalog') {
            this.httpService
              .postCollection(this.collection)
              .pipe(
                mergeMap((response) => {
                  this.uuid = response.package_tenants[0].package_uuid;
                  this.block.properties = { uuid: this.uuid };
                  return this.httpService.updateBlock(
                    environment.cast_id,
                    this.screenID,
                    this.blockID,
                    this.block
                  );
                })
              )
              .subscribe();
          }
          this.routerLink.navigate(['/cast-editor'], {
            queryParams: { screenId: this.screenID },
          });
          this.ngOnInit();
          return EMPTY;
        })
      )
      .subscribe();
  }
  deleted(con: boolean) {
    if (con) this.ngOnInit();
  }
}
