import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CastEditorComponent } from './cast-editor.component';

describe('CastEditorComponent', () => {
  let component: CastEditorComponent;
  let fixture: ComponentFixture<CastEditorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CastEditorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CastEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
