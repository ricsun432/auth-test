import { DragDropModule } from '@angular/cdk/drag-drop';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatCommonModule } from '@angular/material/core';
import { MatDialogModule } from '@angular/material/dialog';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BlockDialogComponent } from './block-dialog/block-dialog.component';
import { CarouselBlockComponent } from './carousel-block/carousel-block.component';
import { CastEditorComponent } from './cast-editor/cast-editor.component';
import { CastRoutingModule } from './cast.routing';
import { CatalogBlockComponent } from './catalog-block/catalog-block.component';
import { CreateScreenComponent } from './create-screen/create-screen.component';
import { DeleteBlockDialogComponent } from './delete-block-dialog/delete-block-dialog.component';
import { DeleteDialogComponent } from './delete-dialog/delete-dialog.component';
import { DeleteResourceDialogComponent } from './delete-resource-dialog/delete-resource-dialog.component';
import { EditScreenComponent } from './edit-screen/edit-screen.component';
import { ScreensComponent } from './screens/screens.component';
import { TextBlockComponent } from './text-block/text-block.component';

@NgModule({
  declarations: [
    BlockDialogComponent,
    CarouselBlockComponent,
    CastEditorComponent,
    CatalogBlockComponent,
    CreateScreenComponent,
    DeleteBlockDialogComponent,
    DeleteDialogComponent,
    EditScreenComponent,
    TextBlockComponent,
    DeleteResourceDialogComponent,
    ScreensComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    CastRoutingModule,
    DragDropModule,
    MatCommonModule,
    MatDialogModule,
    ScrollingModule,
    BrowserAnimationsModule,
    BrowserModule,
    HttpClientModule,
  ],
})
export class CastModule {}
