import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { CastEditorComponent } from '../cast-editor/cast-editor.component';

@Component({
  selector: 'app-block-dialog',
  templateUrl: './block-dialog.component.html',
  styleUrls: ['./block-dialog.component.css'],
})
export class BlockDialogComponent implements OnInit {
  constructor(public dialogRef: MatDialogRef<CastEditorComponent>) {}

  ngOnInit(): void {}

  addCatalog() {
    this.dialogRef.close('catalog');
  }

  addTextBox() {
    this.dialogRef.close('text');
  }

  addCarousel() {
    this.dialogRef.close('carousel');
  }
}
