import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Block } from 'src/app/block';
import { HttpService } from 'src/app/http.service';
import { environment } from 'src/environments/environment';
import { DeleteBlockDialogComponent } from '../delete-block-dialog/delete-block-dialog.component';

@Component({
  selector: 'app-carousel-block',
  templateUrl: './carousel-block.component.html',
  styleUrls: ['./carousel-block.component.css'],
})
export class CarouselBlockComponent implements OnInit {
  constructor(private httpService: HttpService, public dialog: MatDialog) {}
  @Input() content: any = {};
  @Input() screenID: string = '';
  @Input() blockID: number = 1263;
  @Input() block: Block = new Block(0, '', {});
  @Output() deleted = new EventEmitter<boolean>();
  editable: boolean = false;
  requiredImg: string = 'img/jpeg, img/png, img/gif';
  imgUri: string = '';
  submittedImg: boolean = false;
  objectLength: number = 0;
  uploading: boolean = false;
  ngOnInit(): void {
    console.log(typeof this.content);
    console.log(Object.keys(this.content).length);
    this.objectLength = Object.keys(this.content).length;
    console.log(this.content);
  }

  edit() {
    this.editable = true;
  }
  onFileSelected(event: any) {
    this.uploading = true;
    const file: File = event.target.files[0];
    if (file) {
      const formData = new FormData();
      formData.append('file', file);
      if (
        file.type == 'image/gif' ||
        file.type == 'image/jpeg' ||
        file.type == 'image/png'
      ) {
        this.httpService.postFile(formData).subscribe((response) => {
          this.imgUri = response.uri;
          this.submittedImg = true;
          this.uploading = false;
        });
      }
    }
  }
  save() {
    this.editable = false;
    if (this.submittedImg) {
      this.block.properties = {
        items: [{ image: this.imgUri, src: this.imgUri, url: this.imgUri }],
      };
    }

    this.httpService
      .updateBlock(environment.cast_id, this.screenID, this.blockID, this.block)
      .subscribe(() => {
        this.deleted.emit(true);
      });
  }

  del() {
    const dialogRef = this.dialog.open(DeleteBlockDialogComponent, {
      width: '400px',
      height: '130px',
    });
    dialogRef.afterClosed().subscribe((confirm) => {
      if (confirm) {
        console.log('The dialog was closed');

        this.httpService
          .deleteBlock(environment.cast_id, this.screenID, this.blockID)
          .subscribe(() => {
            this.deleted.emit(true);
          });
      }
    });
  }
}
