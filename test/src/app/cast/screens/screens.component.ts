import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { HttpService } from 'src/app/http.service';
import { Screen } from 'src/app/screen';
import { environment } from 'src/environments/environment';
import { CreateScreenComponent } from '../create-screen/create-screen.component';
@Component({
  selector: 'app-screens',
  templateUrl: './screens.component.html',
  styleUrls: ['./screens.component.css'],
})
export class ScreensComponent implements OnInit {
  constructor(
    private httpService: HttpService,
    private routerLink: Router,
    public dialog: MatDialog,
    private router: ActivatedRoute
  ) {
    this.routerLink.routeReuseStrategy.shouldReuseRoute = () => false;
  }
  list: any = [];
  screenID: string = '';
  screen = new Screen('', '', '', 42);

  ngOnInit(): void {
    this.router.queryParams
      .pipe(
        mergeMap((params) => {
          this.screenID = params.screenId;
          return this.httpService.getScreenList(environment.cast_id);
        })
      )
      .subscribe((response) => {
        this.list = response;
        this.list.sort(this.compare);
      });
  }

  compare(a: any, b: any) {
    return a.id - b.id;
  }
  editScreen(id: string) {
    this.routerLink.navigate(['/edit-screen'], {
      queryParams: { screenId: id, screen_id: this.screenID },
    });
  }

  createScreen() {
    const dialogRef = this.dialog.open(CreateScreenComponent, {
      width: '400px',
      height: '150px',
    });
    dialogRef
      .afterClosed()
      .pipe(
        mergeMap((data) => {
          if (data) {
            this.screen.title = data;
            this.screen.description = data.toLowerCase();
            this.screen.slug = data.replaceAll(' ', '-').toLowerCase();
            return this.httpService.postScreen(
              environment.cast_id,
              this.screen
            );
          }
          return EMPTY;
        })
      )
      .subscribe(() => {
        this.routerLink.navigate(['/screens'], {
          queryParams: { screenId: this.screenID },
        });
        this.ngOnInit();
      });
  }

  done() {
    this.routerLink.navigate(['/cast-editor'], {
      queryParams: { screenId: this.screenID },
    });
  }
}
