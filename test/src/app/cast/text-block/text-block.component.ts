import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { Block } from 'src/app/block';
import { HttpService } from 'src/app/http.service';
import { environment } from 'src/environments/environment';
import { DeleteBlockDialogComponent } from '../delete-block-dialog/delete-block-dialog.component';

@Component({
  selector: 'app-text-block',
  templateUrl: './text-block.component.html',
  styleUrls: ['./text-block.component.css'],
})
export class TextBlockComponent implements OnInit {
  editable: boolean = false;
  @Input() content: string = 'test';
  @Input() screenID: string = '';
  @Input() blockID: number = 1263;
  @Input() block: Block = new Block(0, '', {});
  @Output() deleted = new EventEmitter<boolean>();
  constructor(
    private httpService: HttpService,
    private dialog: MatDialog,
    private routerLink: Router
  ) {
    this.routerLink.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  ngOnInit(): void {}
  edit() {
    this.editable = true;
    console.log(this.content);
    console.log(this.screenID);
    console.log(this.blockID);
    console.log(this.block);
  }
  save() {
    this.block.properties = { content: this.content };
    this.editable = false;
    this.httpService
      .updateBlock(environment.cast_id, this.screenID, this.blockID, this.block)
      .subscribe();
  }
  del() {
    const dialogRef = this.dialog.open(DeleteBlockDialogComponent, {
      width: '400px',
      height: '130px',
    });
    dialogRef
      .afterClosed()
      .pipe(
        mergeMap((confirm) => {
          if (confirm) {
            console.log('The dialog was closed');

            return this.httpService.deleteBlock(
              environment.cast_id,
              this.screenID,
              this.blockID
            );
          }
          return EMPTY;
        })
      )
      .subscribe(() => {
        this.deleted.emit(true);
      });
  }
}
