import { Component } from '@angular/core';
//import { ActivatedRoute } from '@angular/router';
//import { JwksValidationHandler, OAuthService } from 'angular-oauth2-oidc';
import { HttpService } from './http.service';
//import { authCodeFlowConfig } from './sso.config';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'test';
  constructor(private httpService: HttpService) {}
}
