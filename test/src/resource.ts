export class Resource {
  constructor(
    public name: string,
    public description: string,
    public type: string,
    public content: string
  ) {}
}
